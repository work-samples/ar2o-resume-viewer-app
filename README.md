# Introduction

This is a sample app I quickly created for a job application (a homework assignment.)

* The app pulls resume (CV) from Firebase real-time database and shows it in a formatted way.
* The resume is stored locally for offline usage or until the new one is fetched from the network.
* The app continually listens to the Firebase changes and updates screen in real time.

It consists of a single activity (meant to be shard between all fragments) and a single fragment.

The fragment uses (Airbnb Epoxy) RecyclerView for showing the resume sections.

The sections toggle (expand/contract) on tap.

# Key implementation aspects

The sample app illustrates the following key points/technologies/design decisions:

    * Kotlin
    * MVVM design pattern
    * DI (with Koin)
    * Data binding
    * Android Architecture components
    * Offline mode support
    * State management
    * RxJava
    * Firebase (real-time database)
    * TDD and unit testing
    * GitLab CI
    * Project structure
    * Clean (hexagonal) Architecture
    * Single activity pattern
    * ...


## TODO

While the implementation is a production-level, the application itself is far from being ready.  Some of the missing MVP features include:

    * User and session management (login/logout)
    * CoordinatorLayout + CollapsingToolbarLayout to react on RecyclerView scrolling
    * Open individual sections in a separate fragment
    * App settings
    * ...





package com.ar2o.resumeviewer

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.ar2o.resumeviewer.feature.MainActivity
import com.ar2o.resumeviewer.feature.common.interactor.interactorModule
import com.ar2o.resumeviewer.feature.common.validator.validatorModule
import com.ar2o.resumeviewer.feature.common.viewmodel.viewModelModule
import com.ar2o.resumeviewer.feature.overview.viewmodel.overviewModule
import com.ar2o.resumeviewer.repository.repositoryModule
import com.ar2o.resumeviewer.scheduler.schedulerModule
import com.ar2o.resumeviewer.stringloader.stringLoaderModule
import com.ar2o.resumeviewer.toast.toasterModule
import com.google.firebase.FirebaseApp
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber
import kotlin.system.exitProcess

class ResumeViewerApp: Application() {

    override fun onCreate() {
        super.onCreate()
        initFirebase()
        setLogging()
        initKoin()
        setExceptionHandler()
    }

    private fun setLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@ResumeViewerApp)
            androidLogger(level = if (BuildConfig.DEBUG) Level.DEBUG else Level.INFO)
            modules(
                listOf(
                    interactorModule,
                    overviewModule,
                    schedulerModule,
                    stringLoaderModule,
                    toasterModule,
                    validatorModule,
                    viewModelModule
                ) + repositoryModule
            )
        }
    }

    private fun initFirebase() {
        FirebaseApp.initializeApp(this)
    }

    private fun setExceptionHandler() {
        if (!BuildConfig.DEBUG) {
            Thread.setDefaultUncaughtExceptionHandler { t, e ->
                Timber.tag("").e(e,"xxxxx DefaultUncaughtExceptionHandler: thread{${t.name}}")
                val intent = Intent(this, MainActivity::class.java).apply {
                    addFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TOP
                                or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                or Intent.FLAG_ACTIVITY_NEW_TASK
                    )
                }
                val pendingIntent = PendingIntent.getActivity(this,
                    0, intent, PendingIntent.FLAG_ONE_SHOT)
                (getSystemService(Context.ALARM_SERVICE) as AlarmManager).apply {
                    setExact(AlarmManager.RTC, System.currentTimeMillis() + 100L, pendingIntent)
                }
                exitProcess(2)
            }
        }
    }
}
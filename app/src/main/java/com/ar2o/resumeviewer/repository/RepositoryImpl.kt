package com.ar2o.resumeviewer.repository

import com.ar2o.resumeviewer.repository.local.LocalRepository
import com.ar2o.resumeviewer.repository.model.Resume
import com.ar2o.resumeviewer.repository.remote.CmsResume
import com.ar2o.resumeviewer.repository.remote.RemoteRepository
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import timber.log.Timber

internal class RepositoryImpl(
    private val remoteRepo: RemoteRepository,
    private val localRepo: LocalRepository

): Repository {

    override fun fetchContent(): Observable<Resume> {
        return Observable.create(ObservableOnSubscribe{
                emitter ->
            if (emitter.isDisposed) return@ObservableOnSubscribe

            Observable.concat(
                fetchLocalContent(),
                fetchRemoteContent()
            ).subscribe(
                {
                    Timber.d("xxxxx fetchContent(): concat().onNext()")
                    emitter.onNext(it.toPublic())
                },
                {
                    Timber.e(it,"xxxxx fetchContent(): concat().onError()")
                }
            )
        })
    }

    private fun fetchRemoteContent(): Observable<CmsResume> {
        return remoteRepo.fetchContent()
            .doOnNext {
                Timber.tag(TAG).v("xxxxx fetchRemoteContent(): onNext(): save to local")
                localRepo.saveResume(it)
            }
    }

    private fun fetchLocalContent(): Observable<CmsResume> {
        return localRepo.fetchResume()
            .doOnNext {
                Timber.tag(TAG).v("xxxxx fetchLocalContent(): onNext()")
            }
    }

    companion object {
        private val TAG = RepositoryImpl::class.java.simpleName
    }
}
package com.ar2o.resumeviewer.repository.remote

import io.reactivex.Observable

interface RemoteRepository {
    fun fetchContent(): Observable<CmsResume>
}
package com.ar2o.resumeviewer.repository.local

import com.ar2o.resumeviewer.repository.remote.CmsResume
import io.reactivex.Completable
import io.reactivex.Observable

interface LocalRepository {
    fun fetchResume(): Observable<CmsResume>
    fun saveResume(resume: CmsResume): Completable
}
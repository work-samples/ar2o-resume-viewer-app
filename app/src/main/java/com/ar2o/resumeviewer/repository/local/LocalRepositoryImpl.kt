package com.ar2o.resumeviewer.repository.local

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.ar2o.resumeviewer.repository.remote.CmsResume
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Observable
import timber.log.Timber
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class LocalRepositoryImpl(
    private val context: Context,
    private val gson: Gson

): LocalRepository {

    override fun fetchResume(): Observable<CmsResume> {
        return if (resume.isBlank()) {
            Timber.tag(TAG).i("xxxxx fetchResume(): no saved resume found => returning empty resume")
            Observable.empty<CmsResume>()
        } else {
            try {
                Timber.tag(TAG).v("xxxxx fetchResume(): saved resume found")
                Observable.just(gson.fromJson(resume, CmsResume::class.java))
            } catch (t: Throwable) {
                Timber.tag(TAG).e(t,"xxxxx fetchResume(): JSON problem => returning empty resume")
                Observable.empty<CmsResume>()
            }
        }
    }

    override fun saveResume(resume: CmsResume): Completable {
        return try {
            this.resume = gson.toJson(resume)
            Completable.complete()
        } catch (t: Throwable) {
            Timber.tag(TAG).e(t,"xxxxx saveResume(): JSON problem => returning error")
            Completable.error{ t }
        }
    }

    private var resume: String by PreferenceDelegate(RESUME_KEY, "")

    private inner class PreferenceDelegate<T>(
        val name: String,
        val default: T
    ) : ReadWriteProperty<Any?, T> {

        private val preference: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

        @Suppress("UNCHECKED_CAST")
        override fun getValue(thisRef: Any?, property: KProperty<*>): T = when (default) {
            is String -> preference.getString(name, default) as T
            is String? -> preference.getString(name, default) as T
            is Int -> preference.getInt(name, default) as T
            is Long -> preference.getLong(name, default) as T
            is Boolean -> preference.getBoolean(name, default) as T
            is Float -> preference.getFloat(name, default) as T
            else -> throw IllegalArgumentException("Invalid type: $default")
        }

        override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
            with(preference.edit()) {
                when (default) {
                    is String -> putString(name, value as String)
                    is String? -> putString(name, value as String?)
                    is Int -> putInt(name, value as Int)
                    is Long -> putLong(name, value as Long)
                    is Boolean -> putBoolean(name, value as Boolean)
                    is Float -> putFloat(name, value as Float)
                    else -> throw IllegalArgumentException("Invalid type: $default")
                }
                apply()
            }
        }
    }

    companion object {
        private val TAG = LocalRepositoryImpl::class.java.simpleName

        private const val RESUME_KEY = "resume"
    }
}

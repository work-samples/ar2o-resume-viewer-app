package com.ar2o.resumeviewer.repository

import com.ar2o.resumeviewer.repository.model.*
import com.ar2o.resumeviewer.repository.remote.*

/////////////////////////////////////////////////////////////////////////////
internal fun CmsPersonAddress.toPublic() = PersonAddress(
    unitNumber   = unitNumber  ,
    streetNumber = streetNumber,
    streetName   = streetName  ,
    city         = city        ,
    province     = province    ,
    postCode     = postCode
)

internal fun CmsPersonContact.toPublic() = PersonContact(
    phone = phone,
    email = email
)

internal fun CmsPerson.toPublic() = Person(
    firstName = firstName,
    lastName  = lastName ,
    title     = title
)

internal fun CmsPersonInfoSection.toPublic() = PersonInfoSection(
    person  = person.toPublic(),
    contact = contact.toPublic(),
    address = address.toPublic()
)

/////////////////////////////////////////////////////////////////////////////
internal fun CmsQualificationsSection.toPublic() = QualificationsSection(
    description = description,
    items       = items
)

/////////////////////////////////////////////////////////////////////////////
internal fun CmsAchievementsSection.toPublic() = AchievementsSection(
    description = description,
    items       = items
)

/////////////////////////////////////////////////////////////////////////////
internal fun CmsEmploymentItemDate.toPublic() = EmploymentItemDate(
    month = month,
    year  = year
)

internal fun CmsEmploymentItemInfo.toPublic() = EmploymentItemInfo(
    title     = title    ,
    company   = company  ,
    city      = city     ,
    state     = state    ,
    country   = country  ,
    startDate = startDate.toPublic(),
    endDate   = endDate.toPublic()
)

internal fun CmsEmploymentItemProject.toPublic() = EmploymentItemProject(
    names       = names      ,
    description = description,
    items       = items
)


internal fun CmsEmploymentItem.toPublic() = EmploymentItem(
    info     = info.toPublic()    ,
    projects = projects.map { it.toPublic() }
)

internal fun CmsEmploymentSection.toPublic() = EmploymentSection(
    items = items.map { it.toPublic() }
)

/////////////////////////////////////////////////////////////////////////////
internal fun CmsTechSkillItem.toPublic() = TechSkillItem(
    name  = name,
    items = items
)

internal fun CmsTechSkillsSection.toPublic() = TechSkillsSection(
    items = items.map { it.toPublic() }
)

/////////////////////////////////////////////////////////////////////////////
internal fun CmsOtherSkillItem.toPublic() = OtherSkillItem(
    name  = name,
    items = items
)

internal fun CmsOtherSkillsSection.toPublic() = OtherSkillsSection(
    items = items.map { it.toPublic() }
)

/////////////////////////////////////////////////////////////////////////////
internal fun CmsEducationItem.toPublic() = EducationItem(
    title       = title      ,
    institution = institution,
    country     = country
)

internal fun CmsEducationSection.toPublic() = EducationSection(
    items = items.map { it.toPublic() }
)

/////////////////////////////////////////////////////////////////////////////
internal fun CmsCourseItem.toPublic() = CourseItem(
    name        = name       ,
    description = description
)

internal fun CmsCoursesSection.toPublic() = CoursesSection(
    items = items.map { it.toPublic() }
)

/////////////////////////////////////////////////////////////////////////////
internal fun CmsBookItem.toPublic() = BookItem(
    name    = name   ,
    authors = authors
)

internal fun CmsBooksSection.toPublic() = BooksSection(
    items = items.map { it.toPublic() }
)

/////////////////////////////////////////////////////////////////////////////
internal fun CmsResume.toPublic() = Resume(
    personInfo     = personInfo.toPublic(),
    qualifications = qualifications.toPublic(),
    achievements   = achievements.toPublic(),
    employment     = employment.toPublic(),
    techSkills     = techSkills.toPublic(),
    otherSkills    = otherSkills.toPublic(),
    education      = education.toPublic(),
    courses        = courses.toPublic(),
    books          = books.toPublic()
)

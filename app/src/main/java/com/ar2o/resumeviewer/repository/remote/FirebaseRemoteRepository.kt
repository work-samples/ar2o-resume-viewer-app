package com.ar2o.resumeviewer.repository.remote

import com.google.firebase.database.*
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import timber.log.Timber

class FirebaseRemoteRepository: RemoteRepository {

    private val fbDb = FirebaseDatabase.getInstance().reference
    private val fbContentNode = fbDb.child(DB_CONTENT_NODE_PATH)

    private val behaviorSubject = BehaviorSubject.create<CmsResume>()

    init {
        fbContentNode.addValueEventListener(makeContentListener(behaviorSubject))
    }

    override fun fetchContent(): Observable<CmsResume> {
        return behaviorSubject
    }

    companion object {
        private const val FB_LISTENER_TAG = "FbContentListener"

        private const val DB_CONTENT_NODE_PATH = "content"

        private fun makeContentListener(subject: Subject<CmsResume>): ValueEventListener {
            return  object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val resume = dataSnapshot.getValue(CmsResume::class.java)
                    Timber.tag(FB_LISTENER_TAG).v("xxxxx contentListener::onDataChange(): resume[${resume}]")
                    resume?.let {
                        subject.onNext(resume)
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Timber.tag(FB_LISTENER_TAG).v("xxxxx contentListener::onCancelled(): resume[${databaseError.message}]")
                }
            }
        }
    }
}
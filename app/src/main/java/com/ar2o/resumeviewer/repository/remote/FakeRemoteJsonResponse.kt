package com.ar2o.resumeviewer.repository.remote

internal object FakeRemoteJsonResponse {

    /////////////////////////////////////////////////////////////////////////////
    const val PERSON_FNAME = "Filip"
    const val PERSON_LNAME = "Petrovic"
    const val PERSON_TITLE = "MScECE"

    const val CONTACT_PHONE = "604-123-4567"
    const val CONTACT_EMAIL = "jdoe@example.com"

    const val ADDRESS_UNIT_NUM    = "42"
    const val ADDRESS_STREET_NUM  = "100"
    const val ADDRESS_STREET_NAME = "Main Street"
    const val ADDRESS_CITY        = "Vancouver"
    const val ADDRESS_PROVINCE    = "BC"
    const val ADDRESS_POST_CODE   = "V6C 3R3"

    val PERSON_INFO_JSON = """
        {
            "person": {
                "firstName": "$PERSON_FNAME",
                "lastName": "$PERSON_LNAME",
                "title": "$PERSON_TITLE"
            },
            "contact": {
                "phone": "$CONTACT_PHONE",
                "email": "$CONTACT_EMAIL"
            },
            "address": {
                "unitNumber": "$ADDRESS_UNIT_NUM",
                "streetNumber": "$ADDRESS_STREET_NUM",
                "streetName": "$ADDRESS_STREET_NAME",
                "city": "$ADDRESS_CITY",
                "province": "$ADDRESS_PROVINCE",
                "postCode": "$ADDRESS_POST_CODE"
            }
        }
    """.trimIndent()


    /////////////////////////////////////////////////////////////////////////////
    const val QUALIFICATIONS_DESCRIPTION = "Creative, versatile, and goal oriented software architect and technical leader with 25 years of successful, progressive experience in all phases of SDLC,  with specialization in mobile applications and systems in the last 10 years – since the Cupcake Android API release."
    const val QUALIFICATIONS_ITEM_0      = "Provided architectural design, guidance, policies, and vision, participated in development of wide range of Android and mobile systems"
    const val QUALIFICATIONS_ITEM_1      = "Hands-on experience in developing Android applications"
    const val QUALIFICATIONS_ITEM_2      = "Lead Android MDM (Mobile Device Management) and BYOD (Bring Your Own Device) initiatives"

    val QUALIFICATIONS_JSON = """
        {
            "description": "$QUALIFICATIONS_DESCRIPTION",
            "items": [ "$QUALIFICATIONS_ITEM_0", "$QUALIFICATIONS_ITEM_1", "$QUALIFICATIONS_ITEM_2" ]
        }
    """.trimIndent()


    /////////////////////////////////////////////////////////////////////////////
    const val ACHIEVEMENTS_DESCRIPTION = ""
    const val ACHIEVEMENTS_ITEM_0      = "Have been involved in Android platform and application development since the first significant Android public release (Cupcake.)"
    const val ACHIEVEMENTS_ITEM_1      = "Designed and implemented components at all Android levels"
    const val ACHIEVEMENTS_ITEM_2      = "Supervised and developed Android enterprise level real-time security and asset"
    const val ACHIEVEMENTS_ITEM_3      = "Created SDKs for Android (and non-Android) application development"

    val ACHIEVEMENTS_JSON = """
        {
            "description": "$ACHIEVEMENTS_DESCRIPTION",
            "items": [ "$ACHIEVEMENTS_ITEM_0", "$ACHIEVEMENTS_ITEM_1", "$ACHIEVEMENTS_ITEM_2", "$ACHIEVEMENTS_ITEM_3" ]
        }
    """.trimIndent()


    /////////////////////////////////////////////////////////////////////////////
    const val EMPLOYMENT_ITEM_0_START_DATE_MONTH      =   10
    const val EMPLOYMENT_ITEM_0_START_DATE_YEAR       = 2017
    const val EMPLOYMENT_ITEM_0_END_DATE_MONTH        =    0 // present
    const val EMPLOYMENT_ITEM_0_END_DATE_YEAR         =    0 // present
    const val EMPLOYMENT_ITEM_0_INFO_TITLE            = "Senior Software Architect"
    const val EMPLOYMENT_ITEM_0_INFO_COMPANY          = "Virtual Visions"
    const val EMPLOYMENT_ITEM_0_INFO_CITY             = "San Francisco"
    const val EMPLOYMENT_ITEM_0_INFO_STATE            = "CA"
    const val EMPLOYMENT_ITEM_0_INFO_COUNTRY          = "USA"
    const val EMPLOYMENT_ITEM_0_PROJECT_0_NAME_0      = "Uniqlo: Ditigal Easel;  Product catalogue / Point Of Paiment (POP) app"
    const val EMPLOYMENT_ITEM_0_PROJECT_0_NAME_1      = "Nordstrom: Stylist Mirror – Virtual Try-On for makeup"
    const val EMPLOYMENT_ITEM_0_PROJECT_0_NAME_2      = "Zales: Digital Easel;  Product catalogue + Virtual Try-On for jewelry"
    const val EMPLOYMENT_ITEM_0_PROJECT_0_NAME_3      = "Virtual Visions: Mobile Device Management (MDM) system"
    const val EMPLOYMENT_ITEM_0_PROJECT_0_DESCRIPTION = "As a Software Architect / Principal Engineer, my tasks included:"
    const val EMPLOYMENT_ITEM_0_PROJECT_0_ITEM_0      = "Initiate, participate discovery sessions with clients, define requirements (JIRA, Google Drive)"
    const val EMPLOYMENT_ITEM_0_PROJECT_0_ITEM_1      = "Define end-to-end system architecture and workflows, suggest and document various policies (e.g. security, PCI, networking, CI.)"
    const val EMPLOYMENT_ITEM_0_PROJECT_0_ITEM_2      = "Design and implement Android applications (AndroidX, Kotlin with coroutines, Android Architecture Components, MVVM with data binding, Dagger, Koin, RxJava, Retrofit, TDD), implemenging complex workflows integrating with third party SDKs (PerfectCorp AR and Visual Analytics SDK, Perfect365 AR SDK, Firebase) and back-end APIs (RESTfull eCommerce and MDM, data feed)"

    const val EMPLOYMENT_ITEM_1_START_DATE_MONTH      =   10
    const val EMPLOYMENT_ITEM_1_START_DATE_YEAR       = 2016
    const val EMPLOYMENT_ITEM_1_END_DATE_MONTH        =   10
    const val EMPLOYMENT_ITEM_1_END_DATE_YEAR         = 2017
    const val EMPLOYMENT_ITEM_1_INFO_TITLE            = "Independent Consultant – Principal Engineer – Android"
    const val EMPLOYMENT_ITEM_1_INFO_COMPANY          = "AR2O Consulting Inc."
    const val EMPLOYMENT_ITEM_1_INFO_CITY             = "Burnaby"
    const val EMPLOYMENT_ITEM_1_INFO_STATE            = "BC"
    const val EMPLOYMENT_ITEM_1_INFO_COUNTRY          = "Canada"

    const val EMPLOYMENT_ITEM_1_PROJECT_0_NAME_0      = "Lululemont DCB (Digital Community Board) – Smart Mirror Android system"
    const val EMPLOYMENT_ITEM_1_PROJECT_0_DESCRIPTION = "Smart Mirror is essentially a large size Android HDTV with reflective and touch-sensitive surface, serving as a mirror as well as an interactive device.  The features include catalogue browsing, Twitter & Instagram integration, real-time product inventory status, RFID scanning and product lookup, per-device media content and feature configuration, to name a few.  My tasks included:"
    const val EMPLOYMENT_ITEM_1_PROJECT_0_ITEM_0      = "Select technologies, define architecture, and implement Android application using TDD, MVP, Clean Architectur, Kotlin, Dagger, RxJava"
    const val EMPLOYMENT_ITEM_1_PROJECT_0_ITEM_1      = "Define architecture and RESTful API for the mirror management (MDM) back-end"
    const val EMPLOYMENT_ITEM_1_PROJECT_0_ITEM_2      = "Integrate Twitter, Instagram, as well as Oracle Responsys email campaign platform"
    const val EMPLOYMENT_ITEM_1_PROJECT_0_ITEM_3      = "Specify technical requirement for the RFID and work with Lululemon and ImpinJ on finding satisfying hardware and software configuration."

    const val EMPLOYMENT_ITEM_1_PROJECT_1_NAME_0      = "Milk Boutique DCB and RFID Changing Room – Smart Mirror Android system"
    const val EMPLOYMENT_ITEM_1_PROJECT_1_DESCRIPTION = "Milk Boutique (www.shopatmilk.com) is a Los Angenles based, high-end, designer women’s clothing retailer. Besides DCB features, similar to those of Lululemon, the project included a sofisticated RFID integration with the store changing rooms and Shopify ecommerce platform. My tasks included:"
    const val EMPLOYMENT_ITEM_1_PROJECT_1_ITEM_0      = "Select technologies, define architecture, and implement Android application using TDD, MVP, Clean Architecture, Kotlin, Dagger, RxJava"
    const val EMPLOYMENT_ITEM_1_PROJECT_1_ITEM_1      = "Implement Shopify ecommerce integration (I designed and implemented is as a plugin)"

    const val EMPLOYMENT_ITEM_1_PROJECT_2_NAME_0      = "Reddit Android App"
    const val EMPLOYMENT_ITEM_1_PROJECT_2_DESCRIPTION = "Reddit is working on adding new functionality (which I am not at libery of disclosing) to their mobile app.  I have been consulting Reddit on selecting the technologies, overall architecure, and transitioning to MVVM, Clean Architecture, Kotlin, and RxJava."

    val EMPLOYMENT_JSON = """
        {
            "items": [
                {
                    "info": {
                        "title"    : "$EMPLOYMENT_ITEM_0_INFO_TITLE",     
                        "company"  : "$EMPLOYMENT_ITEM_0_INFO_COMPANY",  
                        "city"     : "$EMPLOYMENT_ITEM_0_INFO_CITY",     
                        "state"    : "$EMPLOYMENT_ITEM_0_INFO_STATE",    
                        "country"  : "$EMPLOYMENT_ITEM_0_INFO_COUNTRY",  
                        "startDate": { "month": $EMPLOYMENT_ITEM_0_START_DATE_MONTH, "year": $EMPLOYMENT_ITEM_0_START_DATE_YEAR },
                        "endDate"  : { "month": $EMPLOYMENT_ITEM_0_END_DATE_MONTH  , "year": $EMPLOYMENT_ITEM_0_END_DATE_YEAR }
                    },
                    "projects": [
                        {
                            "names": [
                                "$EMPLOYMENT_ITEM_0_PROJECT_0_NAME_0",
                                "$EMPLOYMENT_ITEM_0_PROJECT_0_NAME_1",
                                "$EMPLOYMENT_ITEM_0_PROJECT_0_NAME_2",
                                "$EMPLOYMENT_ITEM_0_PROJECT_0_NAME_3"
                            ],
                            "description": "$EMPLOYMENT_ITEM_0_PROJECT_0_DESCRIPTION",
                            "items": [
                                "$EMPLOYMENT_ITEM_0_PROJECT_0_ITEM_0",
                                "$EMPLOYMENT_ITEM_0_PROJECT_0_ITEM_1",
                                "$EMPLOYMENT_ITEM_0_PROJECT_0_ITEM_2"
                            ]
                        }
                    ]
                },
                {
                    "info": {
                        "title"    : "$EMPLOYMENT_ITEM_1_INFO_TITLE",     
                        "company"  : "$EMPLOYMENT_ITEM_1_INFO_COMPANY",  
                        "city"     : "$EMPLOYMENT_ITEM_1_INFO_CITY",     
                        "state"    : "$EMPLOYMENT_ITEM_1_INFO_STATE",    
                        "country"  : "$EMPLOYMENT_ITEM_1_INFO_COUNTRY",  
                        "startDate": { "month": $EMPLOYMENT_ITEM_1_START_DATE_MONTH, "year": $EMPLOYMENT_ITEM_1_START_DATE_YEAR },
                        "endDate"  : { "month": $EMPLOYMENT_ITEM_1_END_DATE_MONTH  , "year": $EMPLOYMENT_ITEM_1_END_DATE_YEAR }
                    },
                    "projects": [
                        {
                            "names": [ "$EMPLOYMENT_ITEM_1_PROJECT_0_NAME_0" ],
                            "description": "$EMPLOYMENT_ITEM_1_PROJECT_0_DESCRIPTION",
                            "items": [
                                "$EMPLOYMENT_ITEM_1_PROJECT_0_ITEM_0",
                                "$EMPLOYMENT_ITEM_1_PROJECT_0_ITEM_1",
                                "$EMPLOYMENT_ITEM_1_PROJECT_0_ITEM_2",
                                "$EMPLOYMENT_ITEM_1_PROJECT_0_ITEM_3"
                            ]
                        },
                        {
                            "names": [ "$EMPLOYMENT_ITEM_1_PROJECT_1_NAME_0" ],
                            "description": "$EMPLOYMENT_ITEM_1_PROJECT_1_DESCRIPTION",
                            "items": [
                                "$EMPLOYMENT_ITEM_1_PROJECT_1_ITEM_0",
                                "$EMPLOYMENT_ITEM_1_PROJECT_1_ITEM_1"
                            ]
                        },
                        {
                            "names": [ "$EMPLOYMENT_ITEM_1_PROJECT_2_NAME_0" ],
                            "description": "$EMPLOYMENT_ITEM_1_PROJECT_2_DESCRIPTION",
                            "items": []
                        }
                    ]
                }
            ]
        }
    """.trimIndent()


    /////////////////////////////////////////////////////////////////////////////
    const val TECH_SKILLS_ITEM_0_NAME   = "Programming Languages"
    const val TECH_SKILLS_ITEM_0_ITEM_0 = "Kotlin"
    const val TECH_SKILLS_ITEM_0_ITEM_1 = "Java"
    const val TECH_SKILLS_ITEM_0_ITEM_2 = "C++11&14"
    const val TECH_SKILLS_ITEM_0_ITEM_3 = "JavaScript"

    const val TECH_SKILLS_ITEM_1_NAME   = "Methodologies & Tools"
    const val TECH_SKILLS_ITEM_1_ITEM_0 = "Agile (Lean, XP, Scrum, Continuous Integration)"
    const val TECH_SKILLS_ITEM_1_ITEM_1 = "TeamCity"
    const val TECH_SKILLS_ITEM_1_ITEM_2 = "xUnit"

    const val TECH_SKILLS_ITEM_2_NAME   = "Operation Systems"
    const val TECH_SKILLS_ITEM_2_ITEM_0 = "Android"
    const val TECH_SKILLS_ITEM_2_ITEM_1 = "Linux"
    const val TECH_SKILLS_ITEM_2_ITEM_2 = "Windows (desktop, CE, Mobile)"

    const val TECH_SKILLS_ITEM_3_NAME   = "Databases"
    const val TECH_SKILLS_ITEM_3_ITEM_0 = "MongoDB"
    const val TECH_SKILLS_ITEM_3_ITEM_1 = "DynamoDB"
    const val TECH_SKILLS_ITEM_3_ITEM_2 = "SQLite"
    const val TECH_SKILLS_ITEM_3_ITEM_3 = "MS SQL Server"

    val TECH_SKILLS_JSON = """
        {
            "items": [
                {
                    "name": "$TECH_SKILLS_ITEM_0_NAME",
                    "items": [ "$TECH_SKILLS_ITEM_0_ITEM_0", "$TECH_SKILLS_ITEM_0_ITEM_1", "$TECH_SKILLS_ITEM_0_ITEM_2", "$TECH_SKILLS_ITEM_0_ITEM_3" ]
                },
                {
                    "name": "$TECH_SKILLS_ITEM_1_NAME",
                    "items": [ "$TECH_SKILLS_ITEM_1_ITEM_0", "$TECH_SKILLS_ITEM_1_ITEM_1", "$TECH_SKILLS_ITEM_1_ITEM_2" ]
                },
                {
                    "name": "$TECH_SKILLS_ITEM_2_NAME",
                    "items": [ "$TECH_SKILLS_ITEM_2_ITEM_0", "$TECH_SKILLS_ITEM_2_ITEM_1", "$TECH_SKILLS_ITEM_2_ITEM_2" ]
                },
                {
                    "name": "$TECH_SKILLS_ITEM_3_NAME",
                    "items": [ "$TECH_SKILLS_ITEM_3_ITEM_0", "$TECH_SKILLS_ITEM_3_ITEM_1", "$TECH_SKILLS_ITEM_3_ITEM_2", "$TECH_SKILLS_ITEM_3_ITEM_3" ]
                }
            ]
        }
    """.trimIndent()


    /////////////////////////////////////////////////////////////////////////////
    const val OTHER_SKILLS_ITEM_0_NAME   = "Languages"
    const val OTHER_SKILLS_ITEM_0_ITEM_0 = "Fluent in English"
    const val OTHER_SKILLS_ITEM_0_ITEM_1 = "limited in German"
    const val OTHER_SKILLS_ITEM_0_ITEM_2 = "Russian"
    const val OTHER_SKILLS_ITEM_0_ITEM_3 = "mother tongue Serbian"

    const val OTHER_SKILLS_ITEM_1_NAME   = "Sports"
    const val OTHER_SKILLS_ITEM_1_ITEM_0 = "Karate"
    const val OTHER_SKILLS_ITEM_1_ITEM_1 = "Aikido"
    const val OTHER_SKILLS_ITEM_1_ITEM_2 = "Basketball"

    const val OTHER_SKILLS_ITEM_2_NAME   = "Other skills"
    const val OTHER_SKILLS_ITEM_2_ITEM_0 = "Classical singing (first tenor)"
    const val OTHER_SKILLS_ITEM_2_ITEM_1 = "drawing"

    val OTHER_SKILLS_JSON = """
        {
            "items": [
                {
                    "name": "$OTHER_SKILLS_ITEM_0_NAME",
                    "items": [ "$OTHER_SKILLS_ITEM_0_ITEM_0", "$OTHER_SKILLS_ITEM_0_ITEM_1", "$OTHER_SKILLS_ITEM_0_ITEM_2", "$OTHER_SKILLS_ITEM_0_ITEM_3" ]
                },
                {
                    "name": "$OTHER_SKILLS_ITEM_1_NAME",
                    "items": [ "$OTHER_SKILLS_ITEM_1_ITEM_0", "$OTHER_SKILLS_ITEM_1_ITEM_1", "$OTHER_SKILLS_ITEM_1_ITEM_2" ]
                },
                {
                    "name": "$OTHER_SKILLS_ITEM_2_NAME",
                    "items": [ "$OTHER_SKILLS_ITEM_2_ITEM_0", "$OTHER_SKILLS_ITEM_2_ITEM_1" ]
                }
            ]
        }
    """.trimIndent()


    /////////////////////////////////////////////////////////////////////////////
    const val EDUCATION_ITEM_0_TITLE       = "MSc in Electrical and Computer Engineering"
    const val EDUCATION_ITEM_0_INSTITUTION = "University of Belgrade"
    const val EDUCATION_ITEM_0_COUNTRY     = "Serbia"
    const val EDUCATION_ITEM_1_TITLE       = "BSc in Electrical and Computer Engineering"
    const val EDUCATION_ITEM_1_INSTITUTION = "University of Belgrade"
    const val EDUCATION_ITEM_1_COUNTRY     = "Serbia"
    const val EDUCATION_ITEM_2_TITLE       = ""
    const val EDUCATION_ITEM_2_INSTITUTION = "Mathematical High School"
    const val EDUCATION_ITEM_2_COUNTRY     = "Serbia"

    val EDUCATION_JSON = """
        {
            "items": [
                { "title": "$EDUCATION_ITEM_0_TITLE", "institution": "$EDUCATION_ITEM_0_INSTITUTION", "country": "$EDUCATION_ITEM_0_COUNTRY" },
                { "title": "$EDUCATION_ITEM_1_TITLE", "institution": "$EDUCATION_ITEM_1_INSTITUTION", "country": "$EDUCATION_ITEM_1_COUNTRY" },
                { "title": "$EDUCATION_ITEM_2_TITLE", "institution": "$EDUCATION_ITEM_2_INSTITUTION", "country": "$EDUCATION_ITEM_2_COUNTRY" }
            ]
        }
    """.trimIndent()


    /////////////////////////////////////////////////////////////////////////////
    const val COURSES_ITEM_0_NAME        = "Software Architecture"
    const val COURSES_ITEM_0_DESCRIPTION = "by Philippe Kruchten"
    const val COURSES_ITEM_1_NAME        = "Lean Software Development"
    const val COURSES_ITEM_1_DESCRIPTION = "by Mary and Tom Poppendieck"
    const val COURSES_ITEM_2_NAME        = "Architecture and Design Conference"
    const val COURSES_ITEM_2_DESCRIPTION = "Austin, Texas"
    const val COURSES_ITEM_3_NAME        = "The Benefits of Abstraction in Patterns"
    const val COURSES_ITEM_3_DESCRIPTION = "by Linda Rising (UBC)"
    const val COURSES_ITEM_4_NAME        = "AnDevCon – Android Developer Conference"
    const val COURSES_ITEM_4_DESCRIPTION = "Boston, Massachusetts"
    const val COURSES_ITEM_5_NAME        = "GoingNative & CppCon"
    const val COURSES_ITEM_5_DESCRIPTION = "C++11/14 Conferences"

    val COURSES_JSON = """
        {
            "items": [
                { "name": "$COURSES_ITEM_0_NAME", "description": "$COURSES_ITEM_0_DESCRIPTION" },
                { "name": "$COURSES_ITEM_1_NAME", "description": "$COURSES_ITEM_1_DESCRIPTION" },
                { "name": "$COURSES_ITEM_2_NAME", "description": "$COURSES_ITEM_2_DESCRIPTION" },
                { "name": "$COURSES_ITEM_3_NAME", "description": "$COURSES_ITEM_3_DESCRIPTION" },
                { "name": "$COURSES_ITEM_4_NAME", "description": "$COURSES_ITEM_4_DESCRIPTION" },
                { "name": "$COURSES_ITEM_5_NAME", "description": "$COURSES_ITEM_5_DESCRIPTION" }
            ]
        }
    """.trimIndent()

    /////////////////////////////////////////////////////////////////////////////
    const val BOOKS_DESCRIPTION    = "The following books had he key impact on my career and best reflect my views and principles:"
    const val BOOKS_ITEM_0_NAME    = "Pragmatic Programmer"
    const val BOOKS_ITEM_0_AUTHORS = "Andrew Hunt and David Thomas"
    const val BOOKS_ITEM_1_NAME    = "Software Craftsmanship"
    const val BOOKS_ITEM_1_AUTHORS = "Pete McBreen"
    const val BOOKS_ITEM_2_NAME    = "Peopleware"
    const val BOOKS_ITEM_2_AUTHORS = "Tom DeMarco and Timothy Lister"

    val BOOKS_JSON = """
        {
            "description": "$BOOKS_DESCRIPTION",
            "items": [
                { "name": "$BOOKS_ITEM_0_NAME", "authors": "$BOOKS_ITEM_0_AUTHORS" },
                { "name": "$BOOKS_ITEM_1_NAME", "authors": "$BOOKS_ITEM_1_AUTHORS" },
                { "name": "$BOOKS_ITEM_2_NAME", "authors": "$BOOKS_ITEM_2_AUTHORS" }
            ]
        }
    """.trimIndent()

    /////////////////////////////////////////////////////////////////////////////
    val RESUME_JSON = """
        {
            "personInfo"     : $PERSON_INFO_JSON,
            "qualifications" : $QUALIFICATIONS_JSON,
            "achievements"   : $ACHIEVEMENTS_JSON,
            "employment"     : $EMPLOYMENT_JSON,
            "techSkills"     : $TECH_SKILLS_JSON,
            "otherSkills"    : $OTHER_SKILLS_JSON,
            "education"      : $EDUCATION_JSON,
            "courses"        : $COURSES_JSON,
            "books"          : $BOOKS_JSON
        }
    """.trimIndent()

}
package com.ar2o.resumeviewer.repository.remote

import com.google.gson.annotations.SerializedName

/////////////////////////////////////////////////////////////////////////////
data class CmsPersonAddress(
    @field:SerializedName("unitNumber"  ) val unitNumber  : String = "",
    @field:SerializedName("streetNumber") val streetNumber: String = "",
    @field:SerializedName("streetName"  ) val streetName  : String = "",
    @field:SerializedName("city"        ) val city        : String = "",
    @field:SerializedName("province"    ) val province    : String = "",
    @field:SerializedName("postCode"    ) val postCode    : String = ""
)

data class CmsPersonContact(
    @field:SerializedName("phone") val phone: String = "",
    @field:SerializedName("email") val email: String = ""
)

data class CmsPerson(
    @field:SerializedName("firstName") val firstName: String = "",
    @field:SerializedName("lastName" ) val lastName : String = "",
    @field:SerializedName("title"    ) val title    : String = ""
)

data class CmsPersonInfoSection(
    @field:SerializedName("person" ) val person : CmsPerson        = CmsPerson(),
    @field:SerializedName("contact") val contact: CmsPersonContact = CmsPersonContact(),
    @field:SerializedName("address") val address: CmsPersonAddress = CmsPersonAddress()
)

/////////////////////////////////////////////////////////////////////////////
data class CmsQualificationsSection(
    @field:SerializedName("description") val description: String       = "",
    @field:SerializedName("items"      ) val items      : List<String> = emptyList()
)

/////////////////////////////////////////////////////////////////////////////
data class CmsAchievementsSection(
    @field:SerializedName("description") val description: String       = "",
    @field:SerializedName("items"      ) val items      : List<String> = emptyList()
)


/////////////////////////////////////////////////////////////////////////////
data class CmsEmploymentItemDate(
    @field:SerializedName("month") val month: Int = 0,
    @field:SerializedName("year" ) val year : Int = 0
)

data class CmsEmploymentItemInfo(
    @field:SerializedName("title"    ) val title    : String                = "",
    @field:SerializedName("company"  ) val company  : String                = "",
    @field:SerializedName("city"     ) val city     : String                = "",
    @field:SerializedName("state"    ) val state    : String                = "",
    @field:SerializedName("country"  ) val country  : String                = "",
    @field:SerializedName("startDate") val startDate: CmsEmploymentItemDate = CmsEmploymentItemDate(),
    @field:SerializedName("endDate"  ) val endDate  : CmsEmploymentItemDate = CmsEmploymentItemDate()
)

data class CmsEmploymentItemProject(
    @field:SerializedName("names"      ) val names      : List<String> = emptyList(),
    @field:SerializedName("description") val description: String       = "",
    @field:SerializedName("items"      ) val items      : List<String> = emptyList()
)

data class CmsEmploymentItem(
    @field:SerializedName("info"    ) val info    : CmsEmploymentItemInfo          = CmsEmploymentItemInfo(),
    @field:SerializedName("projects") val projects: List<CmsEmploymentItemProject> = emptyList()
)

data class CmsEmploymentSection(
    @field:SerializedName("items") val items: List<CmsEmploymentItem> = emptyList()
)

/////////////////////////////////////////////////////////////////////////////
data class CmsTechSkillItem(
    @field:SerializedName("name" ) val name : String       = "",
    @field:SerializedName("items") val items: List<String> = emptyList()
)

data class CmsTechSkillsSection(
    @field:SerializedName("items") val items: List<CmsTechSkillItem> = emptyList()
)

/////////////////////////////////////////////////////////////////////////////
data class CmsOtherSkillItem(
    @field:SerializedName("name" ) val name : String       = "",
    @field:SerializedName("items") val items: List<String> = emptyList()
)

data class CmsOtherSkillsSection(
    @field:SerializedName("items") val items: List<CmsOtherSkillItem> = emptyList()
)

/////////////////////////////////////////////////////////////////////////////
data class CmsEducationItem(
    @field:SerializedName("title"      ) val title      : String = "",
    @field:SerializedName("institution") val institution: String = "",
    @field:SerializedName("country"    ) val country    : String = ""
)

data class CmsEducationSection(
    @field:SerializedName("items") val items: List<CmsEducationItem> = emptyList()
)

/////////////////////////////////////////////////////////////////////////////
data class CmsCourseItem(
    @field:SerializedName("name"       ) val name       : String = "",
    @field:SerializedName("description") val description: String = ""
)

data class CmsCoursesSection(
    @field:SerializedName("items") val items: List<CmsCourseItem> = listOf()
)

/////////////////////////////////////////////////////////////////////////////
data class CmsBookItem(
    @field:SerializedName("name"   ) val name   : String = "",
    @field:SerializedName("authors") val authors: String = ""
)

data class CmsBooksSection(
    @field:SerializedName("description") val description: String            = "",
    @field:SerializedName("items"      ) val items      : List<CmsBookItem> = listOf()
)

/////////////////////////////////////////////////////////////////////////////
data class CmsResume(
    @field:SerializedName("personInfo"    ) val personInfo     : CmsPersonInfoSection        = CmsPersonInfoSection(),
    @field:SerializedName("qualifications") val qualifications : CmsQualificationsSection    = CmsQualificationsSection(),
    @field:SerializedName("achievements"  ) val achievements   : CmsAchievementsSection      = CmsAchievementsSection(),
    @field:SerializedName("employment"    ) val employment     : CmsEmploymentSection        = CmsEmploymentSection(),
    @field:SerializedName("techSkills"    ) val techSkills     : CmsTechSkillsSection        = CmsTechSkillsSection(),
    @field:SerializedName("otherSkills"   ) val otherSkills    : CmsOtherSkillsSection       = CmsOtherSkillsSection(),
    @field:SerializedName("education"     ) val education      : CmsEducationSection         = CmsEducationSection(),
    @field:SerializedName("courses"       ) val courses        : CmsCoursesSection           = CmsCoursesSection(),
    @field:SerializedName("books"         ) val books          : CmsBooksSection             = CmsBooksSection()
)


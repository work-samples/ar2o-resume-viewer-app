package com.ar2o.resumeviewer.repository

import com.ar2o.resumeviewer.repository.model.Resume
import io.reactivex.Observable

interface Repository {
    fun fetchContent(): Observable<Resume>
}
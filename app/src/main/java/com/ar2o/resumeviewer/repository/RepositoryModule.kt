package com.ar2o.resumeviewer.repository

import com.ar2o.resumeviewer.repository.local.LocalRepository
import com.ar2o.resumeviewer.repository.local.LocalRepositoryImpl
import com.ar2o.resumeviewer.repository.remote.FirebaseRemoteRepository
import com.ar2o.resumeviewer.repository.remote.RemoteRepository
import com.google.gson.Gson
import org.koin.dsl.module

val repositoryModule = module {
    single { Gson() }
    single<RemoteRepository> { FirebaseRemoteRepository() }
    single<LocalRepository> { LocalRepositoryImpl(get(), get())}
    single<Repository> { RepositoryImpl(get(), get())}
}
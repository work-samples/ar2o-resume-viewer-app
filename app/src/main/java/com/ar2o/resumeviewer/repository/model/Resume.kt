package com.ar2o.resumeviewer.repository.model

/////////////////////////////////////////////////////////////////////////////
data class PersonAddress(
    val unitNumber  : String = "",
    val streetNumber: String = "",
    val streetName  : String = "",
    val city        : String = "",
    val province    : String = "",
    val postCode    : String = ""
)

data class PersonContact(
    val phone: String = "",
    val email: String = ""
)

data class Person(
    val firstName: String = "",
    val lastName : String = "",
    val title    : String = ""
)

data class PersonInfoSection (
    val person : Person        = Person(),
    val contact: PersonContact = PersonContact(),
    val address: PersonAddress = PersonAddress()
)

/////////////////////////////////////////////////////////////////////////////
data class QualificationsSection(
    val description: String       = "",
    val items      : List<String> = emptyList()
)

/////////////////////////////////////////////////////////////////////////////
data class AchievementsSection(
    val description: String       = "",
    val items      : List<String> = emptyList()
)

/////////////////////////////////////////////////////////////////////////////
data class EmploymentItemDate(
    val month: Int = 0,
    val year : Int = 0
)

data class EmploymentItemInfo(
    val title    : String         = "",
    val company  : String         = "",
    val city     : String         = "",
    val state    : String         = "",
    val country  : String         = "",
    val startDate: EmploymentItemDate = EmploymentItemDate(),
    val endDate  : EmploymentItemDate = EmploymentItemDate()
)

data class EmploymentItemProject(
    val names      : List<String> = emptyList(),
    val description: String       = "",
    val items      : List<String> = emptyList()
)

data class EmploymentItem(
    val info    : EmploymentItemInfo = EmploymentItemInfo(),
    val projects: List<EmploymentItemProject>  = emptyList()
)

data class EmploymentSection(
    val items: List<EmploymentItem> = emptyList()
)

/////////////////////////////////////////////////////////////////////////////
data class TechSkillItem(
    val name : String       = "",
    val items: List<String> = emptyList()
)

data class TechSkillsSection(
    val items: List<TechSkillItem> = emptyList()
)

/////////////////////////////////////////////////////////////////////////////
data class OtherSkillItem(
    val name : String       = "",
    val items: List<String> = emptyList()
)

data class OtherSkillsSection(
    val items: List<OtherSkillItem> = emptyList()
)

/////////////////////////////////////////////////////////////////////////////
data class EducationItem(
    val title      : String = "",
    val institution: String = "",
    val country    : String = ""
)

data class EducationSection(
    val items: List<EducationItem> = emptyList()
)

/////////////////////////////////////////////////////////////////////////////
data class CourseItem(
    val name       : String = "",
    val description: String = ""
)

data class CoursesSection(
    val items: List<CourseItem> = listOf()
)

/////////////////////////////////////////////////////////////////////////////
data class BookItem(
    val name   : String = "",
    val authors: String = ""
)

data class BooksSection(
    val items: List<BookItem> = listOf()
)

/////////////////////////////////////////////////////////////////////////////
data class Resume (
    val personInfo     : PersonInfoSection        = PersonInfoSection(),
    val qualifications : QualificationsSection    = QualificationsSection(),
    val achievements   : AchievementsSection      = AchievementsSection(),
    val employment     : EmploymentSection        = EmploymentSection(),
    val techSkills     : TechSkillsSection        = TechSkillsSection(),
    val education      : EducationSection         = EducationSection(),
    val courses        : CoursesSection           = CoursesSection(),
    val books          : BooksSection             = BooksSection(),
    val otherSkills    : OtherSkillsSection       = OtherSkillsSection()
)
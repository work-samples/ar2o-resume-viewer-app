package com.ar2o.resumeviewer.repository.remote

import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

internal class FakeRemoteRepository(
    gson: Gson

): RemoteRepository {
    private val publishSubject = BehaviorSubject.create<CmsResume>()

    init {
        publishSubject.onNext(gson.fromJson(FakeRemoteJsonResponse.RESUME_JSON, CmsResume::class.java))
    }

    override fun fetchContent(): Observable<CmsResume> {
        return publishSubject
    }
}
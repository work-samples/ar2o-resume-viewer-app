package com.ar2o.resumeviewer.scheduler

import org.koin.dsl.module

val schedulerModule = module {
    single<SchedulerProvider> { SchedulerProviderImpl() }
}
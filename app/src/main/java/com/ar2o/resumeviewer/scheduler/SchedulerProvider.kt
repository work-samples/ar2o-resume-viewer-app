package com.ar2o.resumeviewer.scheduler

import io.reactivex.Scheduler

interface SchedulerProvider {

    val ioScheduler: Scheduler
    val computationScheduler: Scheduler
    val uiScheduler: Scheduler
}
package com.ar2o.resumeviewer.scheduler

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SchedulerProviderImpl: SchedulerProvider {

    override val ioScheduler: Scheduler = Schedulers.io()
    override val computationScheduler: Scheduler = Schedulers.computation()
    override val uiScheduler: Scheduler = AndroidSchedulers.mainThread()
}
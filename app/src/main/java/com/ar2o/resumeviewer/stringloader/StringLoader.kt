package com.ar2o.resumeviewer.stringloader

import androidx.annotation.StringRes

interface StringLoader {
    fun load(@StringRes id: Int): String
}
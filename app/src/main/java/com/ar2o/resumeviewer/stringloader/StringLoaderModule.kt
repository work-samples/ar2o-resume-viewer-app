package com.ar2o.resumeviewer.stringloader

import org.koin.dsl.module

val stringLoaderModule = module {
    single<StringLoader> { StringLoaderImpl(get())}
}
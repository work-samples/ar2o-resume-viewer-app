package com.ar2o.resumeviewer.stringloader

import android.content.Context
import androidx.annotation.StringRes

class StringLoaderImpl(
    private val context: Context

): StringLoader {

    override fun load(@StringRes id: Int): String  = context.getString(id)
}
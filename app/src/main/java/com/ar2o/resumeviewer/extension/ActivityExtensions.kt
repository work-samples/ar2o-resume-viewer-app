package com.ar2o.resumeviewer.extension

import androidx.annotation.IdRes
import androidx.fragment.app.FragmentActivity

fun FragmentActivity.replaceFragment(
    @IdRes container: Int,
    fragment: androidx.fragment.app.Fragment,
    tag: String,
    addToBackStack: Boolean = false,
    animationType: AnimationType = AnimationType.NONE
) = supportFragmentManager.replaceFragment(
    container,
    fragment,
    tag,
    addToBackStack,
    animationType
)

package com.ar2o.resumeviewer.extension

import androidx.annotation.IdRes
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.ar2o.resumeviewer.R

enum class AnimationType {
    NONE,
    SLIDE_RIGHT,
    SLIDE_BOTTOM,
    FADE
}

fun FragmentManager.replaceFragment(
    @IdRes container: Int,
    fragment: androidx.fragment.app.Fragment,
    tag: String,
    addToBackStack: Boolean = false,
    animationType: AnimationType = AnimationType.NONE
) = beginTransaction()
    .apply { addAnimation(animationType) }
    .replace(container, fragment, tag)
    .apply {
        if (addToBackStack) {
            addToBackStack(tag)
        }
    }
    .commit()

private fun FragmentTransaction.addAnimation(animationType: AnimationType) {
    when (animationType) {
        AnimationType.NONE -> { /* no-op */
        }
        AnimationType.SLIDE_RIGHT -> setCustomAnimations(
            R.anim.fragment_enter_from_right,
            R.anim.fragment_exit_to_left,
            R.anim.fragment_enter_from_left,
            R.anim.fragment_exit_to_right
        )
        AnimationType.SLIDE_BOTTOM -> setCustomAnimations(
            R.anim.fragment_enter_from_bottom,
            R.anim.fragment_exit_to_top,
            R.anim.fragment_enter_from_top,
            R.anim.fragment_exit_to_bottom
        )
        AnimationType.FADE -> setCustomAnimations(
            android.R.anim.fade_in,
            android.R.anim.fade_out,
            android.R.anim.fade_in,
            android.R.anim.fade_out
        )
    }
}

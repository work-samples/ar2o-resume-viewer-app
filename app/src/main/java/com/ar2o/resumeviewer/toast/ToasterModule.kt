package com.ar2o.resumeviewer.toast

import org.koin.dsl.module

val toasterModule = module {
    single<Toaster> { ToasterImpl(get()) }
}
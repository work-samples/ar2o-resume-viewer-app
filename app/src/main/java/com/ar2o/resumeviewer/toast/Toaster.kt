package com.ar2o.resumeviewer.toast

import android.widget.Toast
import androidx.annotation.StringRes

interface Toaster {
    fun toast(@StringRes message: Int, duration: Int = Toast.LENGTH_SHORT)
    fun toast(message: String, duration: Int = Toast.LENGTH_SHORT)
}

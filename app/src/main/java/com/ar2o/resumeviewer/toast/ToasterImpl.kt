package com.ar2o.resumeviewer.toast

import android.content.Context
import androidx.annotation.StringRes
import com.ar2o.resumeviewer.extension.toast

class ToasterImpl (
    private val context: Context
) : Toaster {

    override fun toast(@StringRes message: Int, duration: Int) {
        context.toast(message, duration)
    }

    override fun toast(message: String, duration: Int) {
        context.toast(message, duration)
    }
}
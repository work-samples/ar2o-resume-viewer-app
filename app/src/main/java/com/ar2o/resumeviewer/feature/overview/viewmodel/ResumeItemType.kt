package com.ar2o.resumeviewer.feature.overview.viewmodel

/**
 * Type used for identifying individual elements of the presentation layer.
 */
enum class ResumeItemType {
    ACHIEVEMENTS  ,
    BOOKS         ,
    COURSES       ,
    EDUCATION     ,
    EMPLOYMENT    ,
    OTHER_SKILLS  ,
    PERSON_INFO   ,
    QUALIFICATIONS,
    TECH_SKILLS
}
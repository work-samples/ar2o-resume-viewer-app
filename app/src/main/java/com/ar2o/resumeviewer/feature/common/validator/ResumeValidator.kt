package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.Resume

class ResumeValidator(
    private val achievementsValidator: AchievementsValidator,
    private val booksValidator: BooksValidator,
    private val coursesValidator: CoursesValidator,
    private val educationValidator: EducationValidator,
    private val employmentValidator: EmploymentValidator,
    private val otherSkillsValidator: OtherSkillsValidator,
    private val personInfoValidator: PersonInfoValidator,
    private val qualificationsValidator: QualificationsValidator,
    private val techSkillsValidator: TechSkillsValidator

) {

    fun check(resume: Resume): Unit {
        achievementsValidator.check(resume.achievements)
        booksValidator.check(resume.books)
        coursesValidator.check(resume.courses)
        educationValidator.check(resume.education)
        employmentValidator.check(resume.employment)
        otherSkillsValidator.check(resume.otherSkills)
        personInfoValidator.check(resume.personInfo)
        qualificationsValidator.check(resume.qualifications)
        techSkillsValidator.check(resume.techSkills)
    }
}
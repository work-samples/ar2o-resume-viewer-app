package com.ar2o.resumeviewer.feature.common.interactor

import com.ar2o.resumeviewer.repository.Repository
import com.ar2o.resumeviewer.repository.model.Resume
import io.reactivex.Observable

class GetResumeInteractorImpl(
    private val repository: Repository

): GetResumeInteractor {
    override fun execute(): Observable<Resume> {
        return repository.fetchContent()
    }
}
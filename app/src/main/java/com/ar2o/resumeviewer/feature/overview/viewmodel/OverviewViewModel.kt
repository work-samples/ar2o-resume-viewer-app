package com.ar2o.resumeviewer.feature.overview.viewmodel

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

/**
 * Main feature ViewModel defining the content and behavior
 * of the OverviewFragment elements
 */
abstract class OverviewViewModel: ViewModel(), LifecycleObserver {

    abstract val showDataError: LiveData<Boolean>
    abstract val dataErrorMessage: LiveData<CharSequence>
    abstract val controller: OverviewController
}

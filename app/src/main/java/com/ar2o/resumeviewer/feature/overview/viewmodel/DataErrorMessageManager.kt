package com.ar2o.resumeviewer.feature.overview.viewmodel

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import com.ar2o.resumeviewer.R
import com.ar2o.resumeviewer.stringloader.StringLoader

/**
 * Responsible for creating and formatting (styling) data error message.
 */
internal class DataErrorMessageManager(
    stringLoader: StringLoader

) {
    private val messageCause  = stringLoader.load(R.string.data_error_message_cause)
    private val messageAdvice = stringLoader.load(R.string.data_error_message_advice)

    fun formatMessage(): SpannableString
            = SpannableString(makeMessage()).apply {
                    setSpan(
                        StyleSpan(Typeface.BOLD),
                        0, messageCause.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )}

    private fun makeMessage() = "$messageCause\n\n$messageAdvice"
}

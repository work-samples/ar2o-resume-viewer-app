package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.AchievementsSection

class AchievementsValidator {
    fun check(achievements: AchievementsSection): Unit {
        InternalValidator.checkDescriptionAndItems(
            description = achievements.description,
            items = achievements.items,
            sectionName = "Achievements"
        )
    }
}
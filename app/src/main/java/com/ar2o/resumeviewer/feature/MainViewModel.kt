package com.ar2o.resumeviewer.feature

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel

abstract class MainViewModel : ViewModel(), LifecycleObserver
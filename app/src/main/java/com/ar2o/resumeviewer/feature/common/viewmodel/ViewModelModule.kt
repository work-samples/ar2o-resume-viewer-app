package com.ar2o.resumeviewer.feature.common.viewmodel

import com.ar2o.resumeviewer.feature.MainViewModel
import com.ar2o.resumeviewer.feature.MainViewModelImpl
import com.ar2o.resumeviewer.feature.overview.viewmodel.DataErrorMessageManager
import com.ar2o.resumeviewer.feature.overview.viewmodel.OverviewViewModel
import com.ar2o.resumeviewer.feature.overview.viewmodel.OverviewViewModelImpl
import com.ar2o.resumeviewer.feature.overview.viewmodel.ResumeComposer
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel<MainViewModel> { MainViewModelImpl() }
}
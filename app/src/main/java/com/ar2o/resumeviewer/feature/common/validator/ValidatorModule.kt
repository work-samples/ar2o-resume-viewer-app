package com.ar2o.resumeviewer.feature.common.validator

import org.koin.dsl.module

val validatorModule = module {
    single { AchievementsValidator() }
    single { BooksValidator() }
    single { CoursesValidator() }
    single { EducationValidator() }
    single { EmploymentValidator() }
    single { OtherSkillsValidator() }
    single { PersonInfoValidator() }
    single { QualificationsValidator() }
    single { TechSkillsValidator() }
    single { ResumeValidator(get(), get(), get(), get(), get(), get(), get(), get(), get()) }
}
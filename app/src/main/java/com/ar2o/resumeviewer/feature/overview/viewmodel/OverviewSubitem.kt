package com.ar2o.resumeviewer.feature.overview.viewmodel

import android.os.Parcelable
import com.ar2o.resumeviewer.feature.common.viewmodel.Container
import kotlinx.android.parcel.Parcelize

/**
 * Entity (data source) for OverviewSubitemItemViewModel and OverviewSubitemDetailedViewModel
 */
@Parcelize
data class OverviewSubitem(
    val name: String,
    val detail: String = ""

): Parcelable, Container
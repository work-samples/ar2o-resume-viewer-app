package com.ar2o.resumeviewer.feature

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.ar2o.resumeviewer.R
import com.ar2o.resumeviewer.databinding.ActivityMainBinding
import com.ar2o.resumeviewer.extension.AnimationType
import com.ar2o.resumeviewer.extension.replaceFragment
import com.ar2o.resumeviewer.feature.overview.OverviewFragment
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        lifecycle.addObserver(viewModel)
        if (savedInstanceState == null) {
            showOverview()
        }
    }

    private fun showOverview(slide: Boolean = true) =
        replaceFragment(
            container = R.id.container,
            fragment = OverviewFragment.newInstance(),
            tag = OverviewFragment.FRAGMENT_TAG,
            addToBackStack = true,
            animationType = if (slide) AnimationType.SLIDE_RIGHT else AnimationType.FADE
        )
}

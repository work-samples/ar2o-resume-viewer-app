package com.ar2o.resumeviewer.feature.overview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ar2o.resumeviewer.databinding.FragmentOverviewBinding
import com.ar2o.resumeviewer.feature.overview.viewmodel.OverviewViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class OverviewFragment : Fragment() {

    private val viewModel: OverviewViewModel by viewModel()
    private lateinit var binding: FragmentOverviewBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        if (!::binding.isInitialized) {
            binding = FragmentOverviewBinding.inflate(inflater, container, false)
        }
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewModel = viewModel
        lifecycle.addObserver(viewModel)
    }

    companion object {
        const val FRAGMENT_TAG = "overview_fragment"

        fun newInstance() = OverviewFragment()
    }
}

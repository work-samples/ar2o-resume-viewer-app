package com.ar2o.resumeviewer.feature.common.interactor

import org.koin.dsl.module

val interactorModule = module {
    factory<GetResumeInteractor> { GetResumeInteractorImpl(get())}
}
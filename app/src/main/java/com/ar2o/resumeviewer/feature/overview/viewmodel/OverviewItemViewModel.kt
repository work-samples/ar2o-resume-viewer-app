package com.ar2o.resumeviewer.feature.overview.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.ar2o.resumeviewer.feature.common.viewmodel.ContainerViewModel

/**
 * ViewModel for top level RecyclerView items
 */
class OverviewItemViewModel(
    val item: OverviewItem,
    private val itemCallback: (OverviewItem) -> Unit

): ContainerViewModel<OverviewItem>(item) {

    val id             = item.itemType.ordinal
    val title          = item.title
    val description    = item.description
    val hasDescription = MutableLiveData<Boolean>(item.description.isNotBlank())
    val clickListener  = View.OnClickListener { itemCallback(item) }
    val items          = item.items
}
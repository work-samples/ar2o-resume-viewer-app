package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.EducationSection

class EducationValidator {
    fun check(educationSection: EducationSection): Unit {
        require(educationSection.items.isNotEmpty()) {

            "Missing Education section"
        }

        require(
            educationSection.items.all { it.title.isNotBlank() || it.institution.isNotBlank() }

        ) {

            "Incomplete Education item"
        }
    }
}
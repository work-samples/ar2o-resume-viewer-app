package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.*
import java.util.*


class EmploymentValidator(
    private val currentYear: Int = Calendar.getInstance().get(Calendar.YEAR)
) {
    companion object {
        private val PRESENT_DATE = EmploymentItemDate(month = 0, year = 0)
        private val MIN_DATE_YEAR = 1900
    }

    fun check(employmentSection: EmploymentSection): Unit {
        require(employmentSection.items.isNotEmpty()) {
            "Missing Employment section"
        }
        employmentSection.items.forEach { check(it)}
    }

    private fun check(employmentItem: EmploymentItem): Unit {
        check(employmentItem.info)
        if (employmentItem.projects.isNotEmpty()) {
            employmentItem.projects.forEach { check(it) }
        }
    }

    private fun check(employmentItemInfo: EmploymentItemInfo): Unit {
        require(employmentItemInfo.title.isNotBlank()) {
            "Missing Employment item title"
        }

        require(employmentItemInfo.company.isNotBlank()) {
            "Missing Employment item company"
        }

        check(startDate = employmentItemInfo.startDate, endDate = employmentItemInfo.endDate)
    }

    private fun check(employmentItemProject: EmploymentItemProject): Unit {
        require(employmentItemProject.names.isNotEmpty() && employmentItemProject.names.all { it.isNotBlank() }) {
            "Missing Employment item project name(s)"
        }
    }

    private fun check(startDate: EmploymentItemDate, endDate: EmploymentItemDate): Unit {
        check(startDate)
        if (endDate != PRESENT_DATE) {
            check(endDate)
            require(endDate.normalize() >= startDate.normalize()) {
                "Invalid Employment item date range"
            }
        }
    }

    private fun check(employmentItemDate: EmploymentItemDate): Unit {
        require(employmentItemDate.month in 1..12) {
            "Employment item date month out of range"
        }

        require(employmentItemDate.year in MIN_DATE_YEAR..currentYear) {
            "Employment item date year out of range"
        }
    }

    private fun EmploymentItemDate.normalize() = year * 100 + month
}
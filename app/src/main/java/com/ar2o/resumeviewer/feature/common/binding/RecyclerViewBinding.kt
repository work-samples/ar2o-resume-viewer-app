package com.ar2o.resumeviewer.feature.common.binding

import androidx.databinding.BindingAdapter
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.EpoxyRecyclerView

@BindingAdapter("controller")
fun bindController(epoxyRecyclerView: EpoxyRecyclerView, controller: EpoxyController?) {
    controller?.let {
        epoxyRecyclerView.setController(it)
    }
}

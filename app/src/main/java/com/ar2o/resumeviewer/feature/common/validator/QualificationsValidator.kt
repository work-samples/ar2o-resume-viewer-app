package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.QualificationsSection

class QualificationsValidator {

    fun check(qualifications: QualificationsSection): Unit {
        InternalValidator.checkDescriptionAndItems(
            description = qualifications.description,
            items = qualifications.items,
            sectionName = "Qualifications"
        )
    }
}
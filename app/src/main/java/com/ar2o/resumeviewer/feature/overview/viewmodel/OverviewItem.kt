package com.ar2o.resumeviewer.feature.overview.viewmodel

import android.os.Parcelable
import com.ar2o.resumeviewer.feature.common.viewmodel.Container
import kotlinx.android.parcel.Parcelize

/**
 * Entity (data source) for OverviewItemViewModel
 */
@Parcelize
data class OverviewItem(
    val itemType   : ResumeItemType,
    val title      : String,
    val description: String                = "",
    val isExpanded : Boolean               = false,
    val items      : List<OverviewSubitem> = emptyList()

): Parcelable, Container
package com.ar2o.resumeviewer.feature.common.validator

internal object InternalValidator {
    fun checkDescriptionAndItems(description: String, items: List<String>, sectionName: String): Unit {
        require(
            description.isNotBlank() ||
                    items.areNotBlank()

        ) {
            "Missing $sectionName section"
        }
    }

    fun checkNameWithItems(name: String, items: List<String>, sectionName: String) {
        require(name.isNotBlank()) {
            "Missing $sectionName item name"
        }

        require(items.areNotBlank()) {
            "Missing $sectionName items list"
        }
    }

    private fun List<String>.areNotBlank(): Boolean {
        return isNotEmpty() && any { it.isNotBlank() }
    }


}
package com.ar2o.resumeviewer.feature.overview.viewmodel

import com.ar2o.resumeviewer.R
import com.ar2o.resumeviewer.feature.common.validator.ResumeValidator
import com.ar2o.resumeviewer.repository.model.Resume
import com.ar2o.resumeviewer.stringloader.StringLoader

/**
 * Class encapsulating the policies of translating resume content data
 * fetched from network into (UI) presentation entities.
 */
internal class ResumeComposer(
    private val validator: ResumeValidator,
    private val stringLoader: StringLoader

) {
    fun compose(resume: Resume): List<OverviewItem> {
        check(resume)
        return listOf(
            OverviewItem(
                itemType = ResumeItemType.PERSON_INFO,
                title = "${resume.personInfo.person.firstName} ${resume.personInfo.person.lastName}",
                description = resume.personInfo.person.title,
                items = listOf(
                    OverviewSubitem(name = resume.personInfo.contact.email),
                    OverviewSubitem(name = resume.personInfo.contact.phone)
                )
            ),
            OverviewItem(
                itemType = ResumeItemType.QUALIFICATIONS,
                title = stringLoader.load(R.string.section_qualifications),
                description = resume.qualifications.description,
                items = resume.qualifications.items.map { OverviewSubitem(name = it) }
            ),
            OverviewItem(
                itemType = ResumeItemType.ACHIEVEMENTS,
                title = stringLoader.load(R.string.section_achievements),
                description = resume.achievements.description,
                items = resume.achievements.items.map { OverviewSubitem(name = it) }
            ),
            OverviewItem(
                itemType = ResumeItemType.EMPLOYMENT,
                title = stringLoader.load(R.string.section_employment),
                items = resume.employment.items.map { OverviewSubitem(name = it.info.title, detail = it.info.company) }
            ),
            OverviewItem(
                itemType = ResumeItemType.TECH_SKILLS,
                title = stringLoader.load(R.string.section_tech_skills),
                items = resume.techSkills.items.map { OverviewSubitem(name = it.name, detail = it.items.joinToString(separator = ", ")) }
            ),
            OverviewItem(
                itemType = ResumeItemType.EDUCATION,
                title = stringLoader.load(R.string.section_education),
                items = resume.education.items.map { OverviewSubitem(name = if (it.title.isNotBlank()) it.title else it.institution, detail = it.country) }
            ),
            OverviewItem(
                itemType = ResumeItemType.COURSES,
                title = stringLoader.load(R.string.section_courses),
                items = resume.courses.items.map { OverviewSubitem(name = it.name) }
            ),
            OverviewItem(
                itemType = ResumeItemType.BOOKS,
                title = stringLoader.load(R.string.section_books),
                items = resume.books.items.map { OverviewSubitem(name = it.name, detail = "by ${it.authors}") }
            ),
            OverviewItem(
                itemType = ResumeItemType.OTHER_SKILLS,
                title = stringLoader.load(R.string.section_other_skills),
                items = resume.otherSkills.items.map { OverviewSubitem(name = it.name, detail = it.items.joinToString(separator = ", ")) }
            )
        )
    }

    private fun check(resume: Resume): Unit {
        validator.check(resume)
    }
}

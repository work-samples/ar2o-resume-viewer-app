package com.ar2o.resumeviewer.feature.overview.viewmodel

import com.ar2o.resumeviewer.feature.common.viewmodel.ContainerViewModel

/**
 * ViewModel for a simple second level RecyclerView items.
 */
class OverviewSubitemViewModel(
    private val item: OverviewSubitem

): ContainerViewModel<OverviewSubitem>(item) {

    val name = item.name
}
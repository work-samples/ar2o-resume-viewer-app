package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.BooksSection

class BooksValidator {
    fun check(booksSection: BooksSection): Unit {
        if (booksSection.items.isNotEmpty()) {
            require(booksSection.items.all { it.name.isNotBlank() }) {

                "Missing Books section item name"
            }
        }
    }
}
package com.ar2o.resumeviewer.feature.overview.viewmodel

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.ar2o.resumeviewer.feature.common.interactor.GetResumeInteractor
import com.ar2o.resumeviewer.repository.model.Resume
import com.ar2o.resumeviewer.scheduler.SchedulerProvider
import com.ar2o.resumeviewer.toast.Toaster
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import timber.log.Timber

internal class OverviewViewModelImpl(
    private val getResumeInteractor: GetResumeInteractor,
    private val schedulerProvider: SchedulerProvider,
    private val resumeComposer: ResumeComposer,
    private val dataErrorMessageManager: DataErrorMessageManager,
    private val toaster: Toaster

): OverviewViewModel() {

    override val showDataError = MutableLiveData<Boolean>().apply { value = false }

    override val dataErrorMessage = MutableLiveData<CharSequence>().apply {
        value = dataErrorMessageManager.formatMessage()
    }

    override val controller = OverviewController()

    private val compositeDisposable = CompositeDisposable()

    init {
        fetchResume()
    }

    private fun fetchResume() {
        val observer = newResumeFetchObserver()
        getResumeInteractor
            .execute()
            .subscribeOn(schedulerProvider.computationScheduler)
            .observeOn(schedulerProvider.uiScheduler)
            .subscribe(observer)
        compositeDisposable.add(observer)
    }

    private fun newResumeFetchObserver(): DisposableObserver<Resume> {
        return object : DisposableObserver<Resume>() {
            override fun onNext(resume: Resume) {
                Timber.tag(TAG).v("xxxxx ResumeFetchObserver(): onNext()")
                onResumeFetched(resume)
            }

            override fun onError(e: Throwable) {
                Timber.tag(TAG).e(e, "xxxxx ResumeFetchObserver(): onError()")
                onResumeFetchError(e)
            }

            override fun onComplete() {
                Timber.tag(TAG).w("xxxxx ResumeFetchObserver(): This should never happen")
            }
        }
    }

    private fun onResumeFetched(newResume: Resume) {
        try {
            refreshData(newResume)
            hideDataErrorIfNeeded()

        } catch (e: IllegalArgumentException) {
            Timber.tag(TAG).e(e, "xxxxx onResumeFetched(): Incomplete or otherwise irregular resume data")
            showDataErrorIfNeeded()
        }
    }

    private fun onResumeFetchError(e: Throwable) {
        toaster.shortToast("${e.localizedMessage}")
    }

    private fun onItemClicked(item: OverviewItem) {
        toaster.shortToast("${item.title}")
        controller.currentData?.let { oldItems ->
            val newItems = oldItems.map {
                if (item.itemType.ordinal == it.id) {
                    it.item.copy(isExpanded = !it.item.isExpanded)
                } else {
                    it.item
                }
            }
            controller.setData(newItems.map { OverviewItemViewModel(it, ::onItemClicked) })
        }
    }

    private fun refreshData(resume: Resume) {
        val items = resumeComposer.compose(resume)
        controller.setData(items.map { OverviewItemViewModel(it, ::onItemClicked) })
    }

    private fun showDataErrorIfNeeded() {
        showDataError.value?.let { isShown ->
            if (isShown) {
                Timber.tag(TAG).v("xxxxx showDataErrorIfNeeded(): data error already showing => do nothing")
            } else {
                Timber.tag(TAG).v("xxxxx showDataErrorIfNeeded(): data error not showing => show data error")
                showDataError.postValue(true)
            }
        }
    }

    private fun hideDataErrorIfNeeded() {
        showDataError.value?.let { isShown ->
            if (isShown) {
                Timber.tag(TAG).v("xxxxx hideDataErrorIfNeeded(): data error showing => hide data error")
                showDataError.postValue(false)
            } else {
                Timber.tag(TAG).v("xxxxx hideDataErrorIfNeeded(): data error not showing => do nothing")
            }
        }
    }

    private fun Toaster.shortToast(message: String) = toast(message, Toast.LENGTH_SHORT)

    companion object {
        private val TAG = OverviewViewModel::class.java.simpleName

    }
}
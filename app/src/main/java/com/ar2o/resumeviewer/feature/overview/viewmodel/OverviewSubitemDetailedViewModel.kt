package com.ar2o.resumeviewer.feature.overview.viewmodel

import com.ar2o.resumeviewer.feature.common.viewmodel.ContainerViewModel

/**
 * ViewModel for second level RecyclerView items containing a title and description.
 *
 * Note: this ViewModel is added to illustrate situations of having multiple types of
 * RecyclerView layouts and their ViewModels.
 *
 */
class OverviewSubitemDetailedViewModel(
    private val item: OverviewSubitem

): ContainerViewModel<OverviewSubitem>(item) {

    val name = item.name
    val detail = item.detail
}
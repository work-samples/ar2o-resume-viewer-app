package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.OtherSkillItem
import com.ar2o.resumeviewer.repository.model.OtherSkillsSection

class OtherSkillsValidator {

    fun check(otherSkillsSection: OtherSkillsSection): Unit {
        if (otherSkillsSection.items.isNotEmpty()) {
            otherSkillsSection.items.forEach { check(it) }
        }
    }


    private fun check(otherSkillItem: OtherSkillItem): Unit {
        InternalValidator.checkNameWithItems(
            name = otherSkillItem.name,
            items = otherSkillItem.items,
            sectionName = "OtherSkills"
        )
    }
}
package com.ar2o.resumeviewer.feature.overview.viewmodel

import com.airbnb.epoxy.TypedEpoxyController
import com.ar2o.resumeviewer.overviewItem
import com.ar2o.resumeviewer.overviewSubitem
import com.ar2o.resumeviewer.overviewSubitemDetailed

/**
 * Controller for Epoxy RecyclerView.
 *
 * Responsible for selecting layouts for each RecyclerView individual item,
 * matching corresponding item's content
 */
class OverviewController(): TypedEpoxyController<List<OverviewItemViewModel>>() {

    override fun buildModels(items: List<OverviewItemViewModel>) {
        items.forEach{ item ->
            overviewItem {
                id(item.id)
                viewModel(item)
                onClick(item.clickListener)
            }
            if (item.item.isExpanded) {
                item.applySubitemViewModel()
            }
        }
    }


    private fun OverviewItemViewModel.applySubitemViewModel() {
        if (id in DETAILED_ITEMS) {
            applyDetailedSubitemViewModel()
        } else {
            applySimpleSubitemViewModel()
        }
    }

    private fun OverviewItemViewModel.applyDetailedSubitemViewModel() {
        item.items.forEach {
            overviewSubitemDetailed {
                id(it.name.hashCode())
                viewModel(OverviewSubitemDetailedViewModel(it))
            }
        }
    }

    private fun OverviewItemViewModel.applySimpleSubitemViewModel() {
        items.forEach{
            overviewSubitem {
                id(it.name.hashCode())
                viewModel(OverviewSubitemViewModel(it))
            }
        }
    }

    companion object {
        private val DETAILED_ITEMS = setOf(
            ResumeItemType.EMPLOYMENT.ordinal,
            ResumeItemType.TECH_SKILLS.ordinal,
            ResumeItemType.OTHER_SKILLS.ordinal,
            ResumeItemType.BOOKS.ordinal,
            ResumeItemType.EDUCATION.ordinal
        )
    }
}
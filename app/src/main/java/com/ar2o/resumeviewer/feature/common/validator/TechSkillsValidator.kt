package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.TechSkillItem
import com.ar2o.resumeviewer.repository.model.TechSkillsSection

class TechSkillsValidator {

    fun check(techSkillsSection: TechSkillsSection): Unit {
        if (techSkillsSection.items.isNotEmpty()) {
            techSkillsSection.items.forEach { check(it) }
        }
    }

    private fun check(techSkillItem: TechSkillItem): Unit {
        InternalValidator.checkNameWithItems(
            name = techSkillItem.name,
            items = techSkillItem.items,
            sectionName = "TechSkills"
        )
    }
}
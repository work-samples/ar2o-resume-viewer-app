package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.CoursesSection

class CoursesValidator {
    fun check(coursesSection: CoursesSection): Unit {
        if (coursesSection.items.isNotEmpty()) {
            require(coursesSection.items.all { it.name.isNotBlank() }) {

                "Missing Course section item name"
            }
        }
    }
}
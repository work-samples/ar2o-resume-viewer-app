package com.ar2o.resumeviewer.feature.common.interactor

import com.ar2o.resumeviewer.repository.model.Resume
import io.reactivex.Observable

interface GetResumeInteractor {
    fun execute(): Observable<Resume>
}
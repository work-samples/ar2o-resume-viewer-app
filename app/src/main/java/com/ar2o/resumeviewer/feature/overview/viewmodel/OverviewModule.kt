package com.ar2o.resumeviewer.feature.overview.viewmodel

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Koin module for overview feature.
 *
 * Note: module is created and not in the feature.common.viewmodel.ViewModelModule.kt
 * to improve encapsulation (keep implementation internal)
 */
val overviewModule = module {

    factory { ResumeComposer(get(), get()) }
    factory { DataErrorMessageManager(get()) }
    viewModel<OverviewViewModel> { OverviewViewModelImpl(get(), get(), get(), get(), get()) }
}
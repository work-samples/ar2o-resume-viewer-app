package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.PersonInfoSection

class PersonInfoValidator {
    fun check(personInfo: PersonInfoSection): Unit {
        require(
            personInfo.person.firstName.isNotBlank() &&
                    personInfo.person.lastName.isNotBlank()

        ) {
            "Incomplete or missing person name"
        }

        require(
            personInfo.contact.email.isNotBlank() ||
                    personInfo.contact.phone.isNotBlank()

        ) {
            "Incomplete or missing person contact"
        }

    }
}
package com.ar2o.resumeviewer.feature.common.viewmodel

abstract class ContainerViewModel <T: Container>(
    val container: T

)  {

    override fun equals(other: Any?): Boolean =
        container == (other as? ContainerViewModel<*>)?.container

    override fun hashCode(): Int = container.hashCode()
}

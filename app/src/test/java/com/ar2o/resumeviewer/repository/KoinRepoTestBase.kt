package com.ar2o.resumeviewer.repository

import com.ar2o.resumeviewer.repository.local.LocalRepository
import com.ar2o.resumeviewer.repository.remote.CmsResume
import com.ar2o.resumeviewer.repository.remote.FakeRemoteRepository
import com.ar2o.resumeviewer.repository.remote.RemoteRepository
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Observable
import org.junit.Before
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.test.AutoCloseKoinTest

open class KoinRepoTestBase: AutoCloseKoinTest() {
    @Before
    fun setup() {
        startKoin{
            modules(makeTestModule())
        }
    }

    companion object {
        private fun makeTestModule() = module {
            single { Gson() }
            single<RemoteRepository> { FakeRemoteRepository(get()) }
            single<LocalRepository> { FakeLocalRepository() }
            single<Repository> { RepositoryImpl(get(), get())}
        }

        private class FakeLocalRepository: LocalRepository {
            override fun fetchResume(): Observable<CmsResume> {
                return Observable.empty()
            }

            override fun saveResume(resume: CmsResume): Completable {
                return Completable.complete()
            }
        }
    }
}
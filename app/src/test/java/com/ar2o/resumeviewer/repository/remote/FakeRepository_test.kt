package com.ar2o.resumeviewer.repository.remote

import com.ar2o.resumeviewer.repository.KoinRepoTestBase
import com.ar2o.resumeviewer.repository.remote.TestData_FakeRemoteModelResponse.EXPECTED_RESUME_RESPONSE
import com.google.gson.Gson
import org.junit.Test
import org.koin.test.inject

class FakeRepository_test: KoinRepoTestBase() {
    val gson: Gson by inject()

    /**
     * Verify that the FakeRemoteRepository.fetchContent() Observable never terminates
     * (continues to emit onNext() without ever emitting onComplete or onError)
     */
    @Test
    fun `GIVEN FakeRemoteRepository WHEN client subscribes to fetch content THEN the results are delivered via onNext AND onComplete or onError should not ever happen`() {
        FakeRemoteRepository(gson)
            .fetchContent()
            .test()
            .assertNotComplete()
            .assertNoErrors()
            .assertValue(EXPECTED_RESUME_RESPONSE)
    }
}
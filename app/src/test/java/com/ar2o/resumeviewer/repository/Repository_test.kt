package com.ar2o.resumeviewer.repository

import com.ar2o.resumeviewer.repository.remote.TestData_FakeRemoteModelResponse
import org.junit.Test
import org.koin.test.inject

class Repository_test: KoinRepoTestBase() {

    val repository: Repository by inject()

    /**
     * Verify that the Repository.fetchContent() Observable never terminates
     * (continues to emit onNext() without ever emitting onComplete or onError)
     */
    @Test
    fun `GIVEN Repository WHEN client subscribes to fetch content THEN the results are delivered via onNext AND onComplete or onError should not ever happen`() {
        repository
            .fetchContent()
            .test()
            .assertNotComplete()
            .assertNoErrors()
            .assertValue(TestData_FakeRemoteModelResponse.EXPECTED_RESUME_RESPONSE.toPublic())
    }
}
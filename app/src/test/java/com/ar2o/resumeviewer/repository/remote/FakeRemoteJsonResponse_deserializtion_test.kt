package com.ar2o.resumeviewer.repository.remote

import com.ar2o.resumeviewer.repository.remote.FakeRemoteJsonResponse.ACHIEVEMENTS_JSON
import com.ar2o.resumeviewer.repository.remote.FakeRemoteJsonResponse.BOOKS_JSON
import com.ar2o.resumeviewer.repository.remote.FakeRemoteJsonResponse.COURSES_JSON
import com.ar2o.resumeviewer.repository.remote.FakeRemoteJsonResponse.EDUCATION_JSON
import com.ar2o.resumeviewer.repository.remote.FakeRemoteJsonResponse.EMPLOYMENT_JSON
import com.ar2o.resumeviewer.repository.remote.FakeRemoteJsonResponse.OTHER_SKILLS_JSON
import com.ar2o.resumeviewer.repository.remote.FakeRemoteJsonResponse.PERSON_INFO_JSON
import com.ar2o.resumeviewer.repository.remote.FakeRemoteJsonResponse.QUALIFICATIONS_JSON
import com.ar2o.resumeviewer.repository.remote.FakeRemoteJsonResponse.RESUME_JSON
import com.ar2o.resumeviewer.repository.remote.FakeRemoteJsonResponse.TECH_SKILLS_JSON
import com.ar2o.resumeviewer.repository.remote.TestData_FakeRemoteModelResponse.EXPECTED_ACHIEVEMENTS_RESPONSE
import com.ar2o.resumeviewer.repository.remote.TestData_FakeRemoteModelResponse.EXPECTED_BOOKS_RESPONSE
import com.ar2o.resumeviewer.repository.remote.TestData_FakeRemoteModelResponse.EXPECTED_COURSES_RESPONSE
import com.ar2o.resumeviewer.repository.remote.TestData_FakeRemoteModelResponse.EXPECTED_EDUCATION_RESPONSE
import com.ar2o.resumeviewer.repository.remote.TestData_FakeRemoteModelResponse.EXPECTED_EMPLOYMENT_RESPONSE
import com.ar2o.resumeviewer.repository.remote.TestData_FakeRemoteModelResponse.EXPECTED_OTHER_SKILLS_RESPONSE
import com.ar2o.resumeviewer.repository.remote.TestData_FakeRemoteModelResponse.EXPECTED_PERSON_RESPONSE
import com.ar2o.resumeviewer.repository.remote.TestData_FakeRemoteModelResponse.EXPECTED_QUALIFICATIONS_RESPONSE
import com.ar2o.resumeviewer.repository.remote.TestData_FakeRemoteModelResponse.EXPECTED_RESUME_RESPONSE
import com.ar2o.resumeviewer.repository.remote.TestData_FakeRemoteModelResponse.EXPECTED_TECH_SKILLS_RESPONSE
import com.google.gson.Gson
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class FakeRemoteJsonResponse_deserializtion_test {

    private lateinit var gson: Gson

    @Before
    fun setup() {
        gson = Gson()
    }

    @Test
    fun `GIVEN fully specified Person info section JSON string WHEN deserialized THEN should match expected result`() {
        Assert.assertEquals(
            "Fully specified Person info section JSON should deserialize",
            EXPECTED_PERSON_RESPONSE,
            gson.fromJson(PERSON_INFO_JSON, CmsPersonInfoSection::class.java)
        )
    }

    @Test
    fun `GIVEN fully specified Qualification section JSON string WHEN deserialized THEN should match expected result`() {
        Assert.assertEquals(
            "Fully specified Qualification section JSON should deserialize",
            EXPECTED_QUALIFICATIONS_RESPONSE,
            gson.fromJson(QUALIFICATIONS_JSON, CmsQualificationsSection::class.java)
        )
    }

    @Test
    fun `GIVEN fully specified Achievements section JSON string WHEN deserialized THEN should match expected result`() {
        Assert.assertEquals(
            "Fully specified Achievements section JSON should deserialize",
            EXPECTED_ACHIEVEMENTS_RESPONSE,
            gson.fromJson(ACHIEVEMENTS_JSON, CmsAchievementsSection::class.java)
        )
    }

    @Test
    fun `GIVEN fully specified Employment section JSON string WHEN deserialized THEN should match expected result`() {
        Assert.assertEquals(
            "Fully specified Employment section JSON should deserialize",
            EXPECTED_EMPLOYMENT_RESPONSE,
            gson.fromJson(EMPLOYMENT_JSON, CmsEmploymentSection::class.java)
        )
    }

    @Test
    fun `GIVEN fully specified TechSkills section JSON string WHEN deserialized THEN should match expected result`() {
        Assert.assertEquals(
            "Fully specified TechSkills section JSON should deserialize",
            EXPECTED_TECH_SKILLS_RESPONSE,
            gson.fromJson(TECH_SKILLS_JSON, CmsTechSkillsSection::class.java)
        )
    }

    @Test
    fun `GIVEN fully specified OtherSkills section JSON string WHEN deserialized THEN should match expected result`() {
        Assert.assertEquals(
            "Fully specified OtherSkills section JSON should deserialize",
            EXPECTED_OTHER_SKILLS_RESPONSE,
            gson.fromJson(OTHER_SKILLS_JSON, CmsOtherSkillsSection::class.java)
        )
    }

    @Test
    fun `GIVEN fully specified Education section JSON string WHEN deserialized THEN should match expected result`() {
        Assert.assertEquals(
            "Fully specified Education section JSON should deserialize",
            EXPECTED_EDUCATION_RESPONSE,
            gson.fromJson(EDUCATION_JSON, CmsEducationSection::class.java)
        )
    }

    @Test
    fun `GIVEN fully specified Courses section JSON string WHEN deserialized THEN should match expected result`() {
        Assert.assertEquals(
            "Fully specified Courses section JSON should deserialize",
            EXPECTED_COURSES_RESPONSE,
            gson.fromJson(COURSES_JSON, CmsCoursesSection::class.java)
        )
    }

    @Test
    fun `GIVEN fully specified Books section JSON string WHEN deserialized THEN should match expected result`() {
        Assert.assertEquals(
            "Fully specified Books section JSON should deserialize",
            EXPECTED_BOOKS_RESPONSE,
            gson.fromJson(BOOKS_JSON, CmsBooksSection::class.java)
        )
    }

    @Test
    fun `GIVEN fully specified Resume JSON string WHEN deserialized THEN should match expected result`() {
        Assert.assertEquals(
            "Fully specified Resume JSON should deserialize",
            EXPECTED_RESUME_RESPONSE,
            gson.fromJson(RESUME_JSON, CmsResume::class.java)
        )
    }

}
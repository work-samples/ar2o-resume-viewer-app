package com.ar2o.resumeviewer.repository.remote

internal object TestData_FakeRemoteModelResponse {
    internal val EXPECTED_PERSON_RESPONSE = CmsPersonInfoSection(
        person = CmsPerson(
            firstName = "Filip", lastName = "Petrovic", title = "MScECE"
        ),
        contact = CmsPersonContact(
            phone = "604-123-4567", email = "jdoe@example.com"
        ),
        address = CmsPersonAddress(
            unitNumber = "42", streetNumber = "100", streetName = "Main Street",
            city = "Vancouver", province = "BC", postCode = "V6C 3R3"
        )
    )

    internal val EXPECTED_QUALIFICATIONS_RESPONSE = CmsQualificationsSection(
        description = FakeRemoteJsonResponse.QUALIFICATIONS_DESCRIPTION,
        items = listOf(FakeRemoteJsonResponse.QUALIFICATIONS_ITEM_0, FakeRemoteJsonResponse.QUALIFICATIONS_ITEM_1, FakeRemoteJsonResponse.QUALIFICATIONS_ITEM_2)
    )

    internal val EXPECTED_ACHIEVEMENTS_RESPONSE = CmsAchievementsSection(
        description = FakeRemoteJsonResponse.ACHIEVEMENTS_DESCRIPTION,
        items = listOf(FakeRemoteJsonResponse.ACHIEVEMENTS_ITEM_0, FakeRemoteJsonResponse.ACHIEVEMENTS_ITEM_1, FakeRemoteJsonResponse.ACHIEVEMENTS_ITEM_2, FakeRemoteJsonResponse.ACHIEVEMENTS_ITEM_3)
    )

    internal val EXPECTED_EMPLOYMENT_RESPONSE = CmsEmploymentSection(
        items = listOf(
            CmsEmploymentItem(
                info = CmsEmploymentItemInfo(
                    title     = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_INFO_TITLE,
                    company   = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_INFO_COMPANY,
                    city      = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_INFO_CITY,
                    state     = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_INFO_STATE,
                    country   = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_INFO_COUNTRY,
                    startDate = CmsEmploymentItemDate( month = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_START_DATE_MONTH, year = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_START_DATE_YEAR),
                    endDate   = CmsEmploymentItemDate( month = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_END_DATE_MONTH  , year = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_END_DATE_YEAR )
                ),
                projects = listOf(
                    CmsEmploymentItemProject(
                        names = listOf(
                            FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_PROJECT_0_NAME_0,
                            FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_PROJECT_0_NAME_1,
                            FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_PROJECT_0_NAME_2,
                            FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_PROJECT_0_NAME_3
                        ),
                        description = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_PROJECT_0_DESCRIPTION,
                        items = listOf(
                            FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_PROJECT_0_ITEM_0,
                            FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_PROJECT_0_ITEM_1,
                            FakeRemoteJsonResponse.EMPLOYMENT_ITEM_0_PROJECT_0_ITEM_2
                        )
                    )
                )
            ),
            CmsEmploymentItem(
                info = CmsEmploymentItemInfo(
                    title     = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_INFO_TITLE,
                    company   = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_INFO_COMPANY,
                    city      = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_INFO_CITY,
                    state     = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_INFO_STATE,
                    country   = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_INFO_COUNTRY,
                    startDate = CmsEmploymentItemDate( month = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_START_DATE_MONTH, year = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_START_DATE_YEAR),
                    endDate   = CmsEmploymentItemDate( month = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_END_DATE_MONTH  , year = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_END_DATE_YEAR )
                ),
                projects = listOf(
                    CmsEmploymentItemProject(
                        names = listOf(FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_PROJECT_0_NAME_0),
                        description = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_PROJECT_0_DESCRIPTION,
                        items = listOf(
                            FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_PROJECT_0_ITEM_0,
                            FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_PROJECT_0_ITEM_1,
                            FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_PROJECT_0_ITEM_2,
                            FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_PROJECT_0_ITEM_3
                        )
                    ),
                    CmsEmploymentItemProject(
                        names = listOf(FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_PROJECT_1_NAME_0),
                        description = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_PROJECT_1_DESCRIPTION,
                        items = listOf(
                            FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_PROJECT_1_ITEM_0,
                            FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_PROJECT_1_ITEM_1
                        )
                    ),
                    CmsEmploymentItemProject(
                        names = listOf(FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_PROJECT_2_NAME_0),
                        description = FakeRemoteJsonResponse.EMPLOYMENT_ITEM_1_PROJECT_2_DESCRIPTION
                    )
                )
            )
        )
    )

    internal val EXPECTED_TECH_SKILLS_RESPONSE = CmsTechSkillsSection(
        listOf(
            CmsTechSkillItem(
                name = FakeRemoteJsonResponse.TECH_SKILLS_ITEM_0_NAME,
                items = listOf(FakeRemoteJsonResponse.TECH_SKILLS_ITEM_0_ITEM_0, FakeRemoteJsonResponse.TECH_SKILLS_ITEM_0_ITEM_1, FakeRemoteJsonResponse.TECH_SKILLS_ITEM_0_ITEM_2, FakeRemoteJsonResponse.TECH_SKILLS_ITEM_0_ITEM_3)
            ),
            CmsTechSkillItem(
                name = FakeRemoteJsonResponse.TECH_SKILLS_ITEM_1_NAME,
                items = listOf(FakeRemoteJsonResponse.TECH_SKILLS_ITEM_1_ITEM_0, FakeRemoteJsonResponse.TECH_SKILLS_ITEM_1_ITEM_1, FakeRemoteJsonResponse.TECH_SKILLS_ITEM_1_ITEM_2)
            ),
            CmsTechSkillItem(
                name = FakeRemoteJsonResponse.TECH_SKILLS_ITEM_2_NAME,
                items = listOf(FakeRemoteJsonResponse.TECH_SKILLS_ITEM_2_ITEM_0, FakeRemoteJsonResponse.TECH_SKILLS_ITEM_2_ITEM_1, FakeRemoteJsonResponse.TECH_SKILLS_ITEM_2_ITEM_2)
            ),
            CmsTechSkillItem(
                name = FakeRemoteJsonResponse.TECH_SKILLS_ITEM_3_NAME,
                items = listOf(FakeRemoteJsonResponse.TECH_SKILLS_ITEM_3_ITEM_0, FakeRemoteJsonResponse.TECH_SKILLS_ITEM_3_ITEM_1, FakeRemoteJsonResponse.TECH_SKILLS_ITEM_3_ITEM_2, FakeRemoteJsonResponse.TECH_SKILLS_ITEM_3_ITEM_3)
            )
        )
    )

    internal val EXPECTED_OTHER_SKILLS_RESPONSE = CmsOtherSkillsSection(
        listOf(
            CmsOtherSkillItem(
                name = FakeRemoteJsonResponse.OTHER_SKILLS_ITEM_0_NAME,
                items = listOf(FakeRemoteJsonResponse.OTHER_SKILLS_ITEM_0_ITEM_0, FakeRemoteJsonResponse.OTHER_SKILLS_ITEM_0_ITEM_1, FakeRemoteJsonResponse.OTHER_SKILLS_ITEM_0_ITEM_2, FakeRemoteJsonResponse.OTHER_SKILLS_ITEM_0_ITEM_3)
            ),
            CmsOtherSkillItem(
                name = FakeRemoteJsonResponse.OTHER_SKILLS_ITEM_1_NAME,
                items = listOf(FakeRemoteJsonResponse.OTHER_SKILLS_ITEM_1_ITEM_0, FakeRemoteJsonResponse.OTHER_SKILLS_ITEM_1_ITEM_1, FakeRemoteJsonResponse.OTHER_SKILLS_ITEM_1_ITEM_2)
            ),
            CmsOtherSkillItem(
                name = FakeRemoteJsonResponse.OTHER_SKILLS_ITEM_2_NAME,
                items = listOf(FakeRemoteJsonResponse.OTHER_SKILLS_ITEM_2_ITEM_0, FakeRemoteJsonResponse.OTHER_SKILLS_ITEM_2_ITEM_1)
            )
        )
    )

    internal val EXPECTED_EDUCATION_RESPONSE = CmsEducationSection(
        listOf(
            CmsEducationItem(title = FakeRemoteJsonResponse.EDUCATION_ITEM_0_TITLE, institution = FakeRemoteJsonResponse.EDUCATION_ITEM_0_INSTITUTION, country = FakeRemoteJsonResponse.EDUCATION_ITEM_0_COUNTRY),
            CmsEducationItem(title = FakeRemoteJsonResponse.EDUCATION_ITEM_1_TITLE, institution = FakeRemoteJsonResponse.EDUCATION_ITEM_1_INSTITUTION, country = FakeRemoteJsonResponse.EDUCATION_ITEM_1_COUNTRY),
            CmsEducationItem(title = FakeRemoteJsonResponse.EDUCATION_ITEM_2_TITLE, institution = FakeRemoteJsonResponse.EDUCATION_ITEM_2_INSTITUTION, country = FakeRemoteJsonResponse.EDUCATION_ITEM_2_COUNTRY)
        )
    )

    internal val EXPECTED_COURSES_RESPONSE = CmsCoursesSection(
        listOf(
            CmsCourseItem(name = FakeRemoteJsonResponse.COURSES_ITEM_0_NAME, description = FakeRemoteJsonResponse.COURSES_ITEM_0_DESCRIPTION),
            CmsCourseItem(name = FakeRemoteJsonResponse.COURSES_ITEM_1_NAME, description = FakeRemoteJsonResponse.COURSES_ITEM_1_DESCRIPTION),
            CmsCourseItem(name = FakeRemoteJsonResponse.COURSES_ITEM_2_NAME, description = FakeRemoteJsonResponse.COURSES_ITEM_2_DESCRIPTION),
            CmsCourseItem(name = FakeRemoteJsonResponse.COURSES_ITEM_3_NAME, description = FakeRemoteJsonResponse.COURSES_ITEM_3_DESCRIPTION),
            CmsCourseItem(name = FakeRemoteJsonResponse.COURSES_ITEM_4_NAME, description = FakeRemoteJsonResponse.COURSES_ITEM_4_DESCRIPTION),
            CmsCourseItem(name = FakeRemoteJsonResponse.COURSES_ITEM_5_NAME, description = FakeRemoteJsonResponse.COURSES_ITEM_5_DESCRIPTION)
        )
    )

    internal val EXPECTED_BOOKS_RESPONSE = CmsBooksSection(
        description = FakeRemoteJsonResponse.BOOKS_DESCRIPTION,
        items = listOf(
            CmsBookItem(name = FakeRemoteJsonResponse.BOOKS_ITEM_0_NAME, authors = FakeRemoteJsonResponse.BOOKS_ITEM_0_AUTHORS),
            CmsBookItem(name = FakeRemoteJsonResponse.BOOKS_ITEM_1_NAME, authors = FakeRemoteJsonResponse.BOOKS_ITEM_1_AUTHORS),
            CmsBookItem(name = FakeRemoteJsonResponse.BOOKS_ITEM_2_NAME, authors = FakeRemoteJsonResponse.BOOKS_ITEM_2_AUTHORS)
        )
    )

    internal val EXPECTED_RESUME_RESPONSE = CmsResume(
        personInfo = EXPECTED_PERSON_RESPONSE,
        qualifications = EXPECTED_QUALIFICATIONS_RESPONSE,
        achievements = EXPECTED_ACHIEVEMENTS_RESPONSE,
        employment = EXPECTED_EMPLOYMENT_RESPONSE,
        techSkills = EXPECTED_TECH_SKILLS_RESPONSE,
        otherSkills = EXPECTED_OTHER_SKILLS_RESPONSE,
        education = EXPECTED_EDUCATION_RESPONSE,
        courses = EXPECTED_COURSES_RESPONSE,
        books = EXPECTED_BOOKS_RESPONSE
    )
}
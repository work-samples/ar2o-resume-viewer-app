package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.EmploymentItem
import com.ar2o.resumeviewer.repository.model.EmploymentItemDate
import com.ar2o.resumeviewer.repository.model.EmploymentSection
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.*

class EmploymentValidator_EmploymentInfo_test {

    private lateinit var validator: EmploymentValidator

    @Before
    fun before() {
        validator = EmploymentValidator()
    }



    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItem with empty title WHEN validated THEN should throw exception`() {

        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(title = EMPTY))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItem with blank title WHEN validated THEN should throw exception`() {

        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(title = BLANK))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItem with empty company WHEN validated THEN should throw exception`() {

        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(company = EMPTY))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItem with blank company WHEN validated THEN should throw exception`() {

        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(company = BLANK))
        )))
    }




    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItemInfo with start and end date having same year AND start month is later than end month WHEN validated THEN should throw exception`() {

        val year = 2000
        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                startDate = EmploymentItemDate(month = 10, year = year),
                endDate   = EmploymentItemDate(month =  5, year = year)
            ))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItemInfo with start and end date having same month AND start year is later than end year WHEN validated THEN should throw exception`() {

        val month = 10
        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                startDate = EmploymentItemDate(month = month, year = 2001),
                endDate   = EmploymentItemDate(month = month, year = 2000)
            ))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItemInfo with start date later than end date WHEN validated THEN should throw exception`() {

        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                startDate = EmploymentItemDate(month =  5, year = 2001),
                endDate   = EmploymentItemDate(month = 10, year = 2000)
            ))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItemInfo with start date having 0 for month and non-zero year WHEN validated THEN should throw exception`() {
        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                startDate = EmploymentItemDate(month =  0, year = 2000),
                endDate   = EmploymentItemDate(month = 10, year = 2001)
            ))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItemInfo with start date having negative month WHEN validated THEN should throw exception`() {
        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                startDate = EmploymentItemDate(month = -1, year = 2000),
                endDate   = EmploymentItemDate(month = 10, year = 2001)
            ))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItemInfo with start date having month greater than 12 WHEN validated THEN should throw exception`() {

        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                startDate = EmploymentItemDate(month = 13, year = 2000),
                endDate   = EmploymentItemDate(month = 10, year = 2001)
            ))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItemInfo with start date having year lesser than 1900 WHEN validated THEN should throw exception`() {

        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                startDate = EmploymentItemDate(month = 10, year = 1899),
                endDate   = EmploymentItemDate(month = 10, year = 2001)
            ))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItemInfo with start date having year greater than current year WHEN validated THEN should throw exception`() {

        val currentYear = Calendar.getInstance().get(Calendar.YEAR)
        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                startDate = EmploymentItemDate(month = 10, year = currentYear + 1),
                endDate   = EmploymentItemDate(month = 10, year = 2001)
            ))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItemInfo with start date having year greater than controlled current year WHEN validated THEN should throw exception`() {

        val currentYear = 2000
        val controlledValidator = EmploymentValidator(currentYear = currentYear)
        controlledValidator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                startDate = EmploymentItemDate(month = 10, year = currentYear + 1),
                endDate   = EmploymentItemDate(month = 10, year = 2001)
            ))
        )))
    }


    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItemInfo with end date having 0 for month and non-zero year WHEN validated THEN should throw exception`() {

        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                startDate = EmploymentItemDate(month = 10, year = 2000),
                endDate   = EmploymentItemDate(month =  0, year = 2001)
            ))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItemInfo with end date having year greater than 12 WHEN validated THEN should throw exception`() {

        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                startDate = EmploymentItemDate(month = 10, year = 2000),
                endDate   = EmploymentItemDate(month = 13, year = 2001)
            ))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItemInfo with start date having month 0 and year 0 WHEN validated THEN should throw exception`() {

        validator.check(EmploymentSection(items = listOf(
            EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                startDate = EmploymentItemDate(month =  0, year =    0),
                endDate   = EmploymentItemDate(month = 13, year = 2001)
            ))
        )))
    }

    @Test
    fun `GIVEN EmploymentItemInfo with end date having month 0 and year 0 (meaning Present Date) WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(EmploymentSection(items = listOf(
                EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                    startDate = EmploymentItemDate(month = 10, year = 2000),
                    endDate   = EmploymentItemDate(month =  0, year =    0)
                ))
            )))
        } catch (t: Throwable) {
            Assert.fail("Employment item end date month[0], year[0] means PRESENT DATE and should be valid")
        }
    }

    @Test
    fun `GIVEN EmploymentItemInfo with same start and end date WHEN validated THEN should NOT throw exception`() {

        val employmentItemDate = EmploymentItemDate(month = 10, year = 2000)
        try {
            validator.check(EmploymentSection(items = listOf(
                EmploymentItem(info = EMPLOYMENT_ITEM_INFO.copy(
                    startDate = employmentItemDate,
                    endDate   = employmentItemDate
                ))
            )))
        } catch (t: Throwable) {
            Assert.fail("Employment item same start and end date should be valid")
        }
    }



    @Test
    fun `GIVEN fully specified EmploymentItemInfo WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(EmploymentSection(items = listOf(
                EmploymentItem(info = EMPLOYMENT_ITEM_INFO)
            )))
        } catch (t: Throwable) {
            Assert.fail("Full Employment item should be valid")
        }
    }
}
package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.CourseItem
import com.ar2o.resumeviewer.repository.model.CoursesSection
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class CoursesValidator_test {

    private lateinit var validator: CoursesValidator

    @Before
    fun before() {
        validator = CoursesValidator()
    }

    @Test
    fun `GIVEN empty Courses WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(CoursesSection())

        } catch (t: Throwable) {
            Assert.fail("Empty Courses should be acceptable")
        }
    }


    @Test
    fun `GIVEN Courses having items with no descriptions WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(CoursesSection(items = listOf(
                CourseItem(name = "Name 0"),
                CourseItem(name = "Name 1")
            )))

        } catch (t: Throwable) {
            Assert.fail("Empty Courses should be acceptable")
        }
    }

    @Test
    fun `GIVEN fully specified Courses WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(CoursesSection(items = listOf(
                CourseItem(name = "Name 0", description = "Description 0"),
                CourseItem(name = "Name 1", description = "Description 1")
            )))

        } catch (t: Throwable) {
            Assert.fail("Empty Courses should be acceptable")
        }
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Courses having item with empty name WHEN validated THEN should throw exception`() {

        validator.check(CoursesSection(items = listOf(
            CourseItem(name = "Name 0"),
            CourseItem(name = EMPTY, description = "Description")
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Courses having item with blank name WHEN validated THEN should throw exception`() {

        validator.check(CoursesSection(items = listOf(
            CourseItem(name = "Name 0"),
            CourseItem(name = BLANK, description = "Description")
        )))
    }
}
package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.BookItem
import com.ar2o.resumeviewer.repository.model.BooksSection
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class BooksValidator_test {

    private lateinit var validator: BooksValidator

    @Before
    fun before() {
        validator = BooksValidator()
    }

    @Test
    fun `GIVEN empty Books WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(BooksSection())

        } catch (t: Throwable) {
            Assert.fail("Empty Books should be acceptable")
        }
    }


    @Test
    fun `GIVEN Books having items with no authors WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(BooksSection(items = listOf(
                BookItem(name = "Name 0"),
                BookItem(name = "Name 1")
            )))

        } catch (t: Throwable) {
            Assert.fail("Empty Books should be acceptable")
        }
    }

    @Test
    fun `GIVEN fully specified Books WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(BooksSection(items = listOf(
                BookItem(name = "Name 0", authors = "Authors 0"),
                BookItem(name = "Name 1", authors = "Authors 1")
            )))

        } catch (t: Throwable) {
            Assert.fail("Empty Books should be acceptable")
        }
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Books having item with empty name WHEN validated THEN should throw exception`() {

        validator.check(BooksSection(items = listOf(
            BookItem(name = "Name 0"),
            BookItem(name = EMPTY, authors = "Authors")
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Books having item with blank name WHEN validated THEN should throw exception`() {

        validator.check(BooksSection(items = listOf(
            BookItem(name = "Name 0"),
            BookItem(name = BLANK, authors = "Authors")
        )))
    }
}
package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.Person
import com.ar2o.resumeviewer.repository.model.PersonInfoSection
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class PersonValidator_Person_test {

    companion object {
        private fun newPersonInfoSection(person: Person): PersonInfoSection {
            return PERSON_INFO.copy(person = person)
        }
    }

    private lateinit var validator: PersonInfoValidator

    @Before
    fun before() {
        validator = PersonInfoValidator()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN empty Person WHEN validated THEN should throw exception`() {

        validator.check(
            PersonInfoSection())
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Person with blank first and last name WHEN validated THEN should throw exception`() {

        validator.check(
            newPersonInfoSection(person = PERSON.copy(firstName = BLANK, lastName = BLANK)))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Person with empty first name WHEN validated THEN should throw exception`() {

        validator.check(
            newPersonInfoSection(person = PERSON.copy(firstName = EMPTY)))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Person with blank first name WHEN validated THEN should throw exception`() {

        validator.check(
            newPersonInfoSection(person = PERSON.copy(firstName = BLANK)))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Person with empty last name WHEN validated THEN should throw exception`() {

        validator.check(
            newPersonInfoSection(person = PERSON.copy(lastName = EMPTY)))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Person with blank last name WHEN validated THEN should throw exception`() {

        validator.check(
            newPersonInfoSection(person = PERSON.copy(lastName = BLANK)))
    }

    @Test
    fun `GIVEN Person with blank title WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(
                newPersonInfoSection(person = PERSON.copy(title = BLANK)))

        } catch (t: Throwable) {
            Assert.fail("Person with missing title should be valid")
        }
    }

    @Test
    fun `GIVEN fully specified Person WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(
                newPersonInfoSection(person = PERSON))

        } catch (t: Throwable) {
            Assert.fail("Person with first and last name should be valid")
        }
    }
}
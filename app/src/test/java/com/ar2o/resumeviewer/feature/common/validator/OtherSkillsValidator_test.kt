package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.OtherSkillItem
import com.ar2o.resumeviewer.repository.model.OtherSkillsSection
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class OtherSkillsValidator_test {

    private lateinit var validator: OtherSkillsValidator

    @Before
    fun before() {
        validator = OtherSkillsValidator()
    }

    @Test
    fun `GIVEN empty OtherSkills WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(OtherSkillsSection())
        } catch (t: Throwable) {
            Assert.fail("Empty OtherSkills should be acceptable")
        }
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN OtherSkills with item having empty name WHEN validated THEN should throw exception`() {

        validator.check(OtherSkillsSection(items = listOf(
            OtherSkillItem(name = "Skill 0", items = listOf("Item 00", "Item 01")),
            OtherSkillItem(name = EMPTY    , items = listOf("Item 10", "Item 11"))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN OtherSkills with item having blank name WHEN validated THEN should throw exception`() {

        validator.check(OtherSkillsSection(items = listOf(
            OtherSkillItem(name = "Skill 0", items = listOf("Item 00", "Item 01")),
            OtherSkillItem(name = BLANK    , items = listOf("Item 10", "Item 11"))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN OtherSkills with item having empty item list WHEN validated THEN should throw exception`() {

        validator.check(OtherSkillsSection(items = listOf(
            OtherSkillItem(name = "Skill 0", items = listOf("Item 00", "Item 01")),
            OtherSkillItem(name = "Skill 1", items = emptyList())
        )))
    }

    @Test
    fun `GIVEN fully specified OtherSkills WHEN validated THEN should throw exception`() {

        try {
            validator.check(OtherSkillsSection(items = listOf(
                OtherSkillItem(name = "Skill 0", items = listOf("Item 00", "Item 01")),
                OtherSkillItem(name = "Skill 1", items = listOf("Item 10", "Item 11"))
            )))
        } catch (t: Throwable) {
            Assert.fail("OtherSkills with fully specified items should be valid")
        }
    }
}
package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.EmploymentItem
import com.ar2o.resumeviewer.repository.model.EmploymentSection
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class EmploymentValidator_test {

    private lateinit var validator: EmploymentValidator

    @Before
    fun before() {
        validator = EmploymentValidator()
    }

    @Test
    fun `GIVEN fully specified Employment WHEN validated THEN should NOT throw exception`() {
        try {
            validator.check(EmploymentSection(items = listOf(
                EmploymentItem(info = EMPLOYMENT_ITEM_INFO, projects = listOf(EMPLOYMENT_ITEM_PROJECT))
            )))
        } catch (t: Throwable) {
            Assert.fail("Fully specified Employment section should be valid")
        }
    }
}
package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.PersonContact
import com.ar2o.resumeviewer.repository.model.PersonInfoSection
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class PersonValidator_Contact_test {

    companion object {
        private fun newPersonInfoSection(contact: PersonContact): PersonInfoSection {
            return PERSON_INFO.copy(contact = contact)
        }
    }

    private lateinit var validator: PersonInfoValidator

    @Before
    fun before() {
        validator = PersonInfoValidator()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN empty PersonContact WHEN validated THEN should throw exception`() {

        validator.check(
            PersonInfoSection()
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN PersonContact with blank email and blank phone WHEN validated THEN should throw exception`() {

        validator.check(
            newPersonInfoSection(contact = PERSON_CONTACT.copy(email = BLANK, phone = BLANK)))
    }

    @Test
    fun `GIVEN PersonContact with only email WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(
                newPersonInfoSection(contact = PERSON_CONTACT.copy(phone = EMPTY)))

        } catch (t: Throwable) {
            Assert.fail("PersonContact with only email should be valid")
        }
    }

    @Test
    fun `GIVEN PersonContact with only phone WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(
                newPersonInfoSection(contact = PERSON_CONTACT.copy(email = EMPTY)))

        } catch (t: Throwable) {
            Assert.fail("PersonContact with only phone should be valid")
        }
    }

    @Test
    fun `GIVEN fully specified PersonContact WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(
                newPersonInfoSection(contact = PERSON_CONTACT))

        } catch (t: Throwable) {
            Assert.fail("PersonContact with email and phone should be valid")
        }
    }
}
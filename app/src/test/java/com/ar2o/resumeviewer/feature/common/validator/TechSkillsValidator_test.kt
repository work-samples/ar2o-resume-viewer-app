package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.TechSkillItem
import com.ar2o.resumeviewer.repository.model.TechSkillsSection
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class TechSkillsValidator_test {

    private lateinit var validator: TechSkillsValidator

    @Before
    fun before() {
        validator = TechSkillsValidator()
    }

    @Test
    fun `GIVEN empty TechSkills WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(TechSkillsSection())
        } catch (t: Throwable) {
            Assert.fail("Empty TechSkills should be acceptable")
        }
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN TechSkills with item having empty name WHEN validated THEN should throw exception`() {

        validator.check(TechSkillsSection(items = listOf(
            TechSkillItem(name = "Skill 0", items = listOf("Item 00", "Item 01")),
            TechSkillItem(name = EMPTY    , items = listOf("Item 10", "Item 11"))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN TechSkills with item having blank name WHEN validated THEN should throw exception`() {

        validator.check(TechSkillsSection(items = listOf(
            TechSkillItem(name = "Skill 0", items = listOf("Item 00", "Item 01")),
            TechSkillItem(name = BLANK    , items = listOf("Item 10", "Item 11"))
        )))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN TechSkills with item having empty item list WHEN validated THEN should throw exception`() {

        validator.check(TechSkillsSection(items = listOf(
            TechSkillItem(name = "Skill 0", items = listOf("Item 00", "Item 01")),
            TechSkillItem(name = "Skill 1", items = emptyList())
        )))
    }

    @Test
    fun `GIVEN fully specified TechSkills WHEN validated THEN should throw exception`() {

        try {
            validator.check(TechSkillsSection(items = listOf(
                TechSkillItem(name = "Skill 0", items = listOf("Item 00", "Item 01")),
                TechSkillItem(name = "Skill 1", items = listOf("Item 10", "Item 11"))
            )))
        } catch (t: Throwable) {
            Assert.fail("TechSkills with fully specified items should be valid")
        }
    }
}
package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.QualificationsSection
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class QualificationsValidator_test {

    private lateinit var validator: QualificationsValidator

    @Before
    fun before() {
        validator = QualificationsValidator()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN empty Qualifications WHEN validated THEN should throw exception`() {

        validator.check(QualificationsSection())
    }


    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Qualifications with blank description WHEN validated THEN should throw exception`() {

        validator.check(QualificationsSection(
            description = BLANK
        ))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Qualifications with empty items WHEN validated THEN should throw exception`() {

        validator.check(QualificationsSection(
            items = listOf(EMPTY, EMPTY, EMPTY)
        ))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Qualifications with blank items WHEN validated THEN should throw exception`() {

        validator.check(QualificationsSection(
            items = listOf(BLANK, BLANK, BLANK)
        ))
    }

    @Test
    fun `GIVEN Qualifications with description and empty items WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(QUALIFICATIONS.copy(items = emptyList()))
        } catch (t: Throwable) {
            Assert.fail("Qualifications with only description should be valid")
        }
    }

    @Test
    fun `GIVEN Qualifications with empty description and non-empty items WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(QUALIFICATIONS.copy(description = EMPTY))
        } catch (t: Throwable) {
            Assert.fail("Qualifications with only items should be valid")
        }
    }

    @Test
    fun `GIVEN Qualifications with at least one non-empty item WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(
                QualificationsSection(
                description = EMPTY,
                items = listOf(BLANK, "Item 1", EMPTY, BLANK))
            )
        } catch (t: Throwable) {
            Assert.fail("Qualifications with at least one non-empty items should be valid")
        }
    }

    @Test
    fun `GIVEN Qualifications with non-empty description and non-empty items WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(QUALIFICATIONS)
        } catch (t: Throwable) {
            Assert.fail("Qualifications with description and items should be valid")
        }
    }
}
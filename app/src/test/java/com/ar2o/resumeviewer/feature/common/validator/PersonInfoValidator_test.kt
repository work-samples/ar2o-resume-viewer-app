package com.ar2o.resumeviewer.feature.common.validator

import org.junit.Assert
import org.junit.Before
import org.junit.Test

class PersonInfoValidator_test {

    private lateinit var validator: PersonInfoValidator

    @Before
    fun before() {
        validator = PersonInfoValidator()
    }

    @Test
    fun `GIVEN fully specified PersonInfo WHEN validated THEN should NOT throw exception`() {
        try {
            validator.check(PERSON_INFO)

        } catch (t: Throwable) {
            Assert.fail("Fully specified PersonInfo section should be valid")
        }
    }
}
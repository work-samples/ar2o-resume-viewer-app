package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.EmploymentItem
import com.ar2o.resumeviewer.repository.model.EmploymentItemProject
import com.ar2o.resumeviewer.repository.model.EmploymentSection
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class EmploymentValidator_ProjectItem_test {

    companion object {

        private fun newEmploymentSection(project: EmploymentItemProject): EmploymentSection {
            return EmploymentSection(items = listOf(
                EmploymentItem(
                    info = EMPLOYMENT_ITEM_INFO,
                    projects = listOf(project)
            )))
        }
    }

    private lateinit var validator: EmploymentValidator

    @Before
    fun before() {
        validator = EmploymentValidator()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN EmploymentItemProject with empty project names WHEN validated THEN should throw exception`() {

        validator.check(newEmploymentSection(
            EMPLOYMENT_ITEM_PROJECT.copy(names = emptyList())))
    }

    @Test
    fun `GIVEN EmploymentItemProject with non-empty names and items AND blank description WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(newEmploymentSection(
                    EMPLOYMENT_ITEM_PROJECT.copy(description = BLANK)))

        } catch (t: Throwable) {
            Assert.fail("Employment item project with missing description should be valid")
        }
    }

    @Test
    fun `GIVEN EmploymentItemProject with non-empty names and description AND empty items WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(newEmploymentSection(
                    EMPLOYMENT_ITEM_PROJECT.copy(items = emptyList())))

        } catch (t: Throwable) {
            Assert.fail("Employment item project with missing items should be valid")
        }
    }

    @Test
    fun `GIVEN EmploymentItemProject with non-empty names AND empty items and description WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(newEmploymentSection(
                    EMPLOYMENT_ITEM_PROJECT.copy(
                        description = EMPTY, items = emptyList())))

        } catch (t: Throwable) {
            Assert.fail("Employment item project with missing description and items should be valid")
        }
    }

    @Test
    fun `GIVEN fully specified EmploymentItemProject WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(newEmploymentSection(
                    EMPLOYMENT_ITEM_PROJECT))

        } catch (t: Throwable) {
            Assert.fail("Full Employment item project should be valid")
        }
    }

}
package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.AchievementsSection
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class AchievementsValidator_test {

    private lateinit var validator: AchievementsValidator

    @Before
    fun before() {
        validator = AchievementsValidator()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN empty Achievements WHEN validated THEN should throw exception`() {

        validator.check(AchievementsSection())
    }


    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Achievements with blank description WHEN validated THEN should throw exception`() {

        validator.check(AchievementsSection(
            description = BLANK
        ))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Achievements with empty items WHEN validated THEN should throw exception`() {

        validator.check(AchievementsSection(
            items = listOf(EMPTY, EMPTY, EMPTY)
        ))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Achievements with blank items WHEN validated THEN should throw exception`() {

        validator.check(AchievementsSection(
            items = listOf(BLANK, BLANK, BLANK)
        ))
    }

    @Test
    fun `GIVEN Achievements having only description WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(ACHIEVEMENTS.copy(items = emptyList()))
        } catch (t: Throwable) {
            Assert.fail("Achievements with only description should be valid")
        }
    }

    @Test
    fun `GIVEN Achievements having only items WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(ACHIEVEMENTS.copy(description = EMPTY))
        } catch (t: Throwable) {
            Assert.fail("Achievements with only items should be valid")
        }
    }

    @Test
    fun `GIVEN Achievements having at least one non-empty item WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(
                AchievementsSection(
                description = "",
                items = listOf(BLANK, "Item 1", EMPTY, BLANK))
            )
        } catch (t: Throwable) {
            Assert.fail("Achievements with at least one non-empty items should be valid")
        }
    }

    @Test
    fun `GIVEN fully specified Achievements WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(ACHIEVEMENTS)
        } catch (t: Throwable) {
            Assert.fail("Achievements with description and items should be valid")
        }
    }
}
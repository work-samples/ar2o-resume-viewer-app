package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.EducationItem
import com.ar2o.resumeviewer.repository.model.EducationSection
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class EducationValidator_test {

    private lateinit var validator: EducationValidator

    @Before
    fun before() {
        validator = EducationValidator()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN empty Education WHEN validated THEN should throw exception`() {

        validator.check(EducationSection())
    }


    @Test(expected = IllegalArgumentException::class)
    fun `GIVEN Education with items having only countries WHEN validated THEN should throw exception`() {

        validator.check(EducationSection(items = listOf(
            EducationItem(country = "Canada"),
            EducationItem(country = "USA")
        )))
    }

    @Test
    fun `GIVEN Education with items having only titles WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(EducationSection(items = listOf(
                EducationItem(title = "Title 0"),
                EducationItem(title = "Title 1")
            )))
        } catch (t: Throwable) {
            Assert.fail("Education items with missing title only is valid")
        }
    }

    @Test
    fun `GIVEN Education with items having only titles and countries WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(EducationSection(items = listOf(
                EducationItem(title = "Title 0", country = "Canada"),
                EducationItem(title = "Title 1", country = "USA")
            )))
        } catch (t: Throwable) {
            Assert.fail("Education items with missing institution only is valid")
        }
    }

    @Test
    fun `GIVEN Education with items having only institutions WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(EducationSection(items = listOf(
                EducationItem(institution = "Institution 0"),
                EducationItem(institution = "Institution 1")
            )))
        } catch (t: Throwable) {
            Assert.fail("Education items with missing title only is valid")
        }
    }

    @Test
    fun `GIVEN Education with items having only institutions and countries WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(EducationSection(items = listOf(
                EducationItem(institution = "Institution 0", country = "Canada"),
                EducationItem(institution = "Institution 1", country = "USA")
            )))
        } catch (t: Throwable) {
            Assert.fail("Education items with missing title only is valid")
        }
    }


    @Test
    fun `GIVEN Education with items having only titles and institutions WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(EducationSection(items = listOf(
                EducationItem(title = "Title 0", institution = "Institution 0"),
                EducationItem(title = "Title 1", institution = "Institution 1")
            )))
        } catch (t: Throwable) {
            Assert.fail("Education items with missing institution only is valid")
        }
    }

    @Test
    fun `GIVEN Education with fully specified items WHEN validated THEN should NOT throw exception`() {

        try {
            validator.check(EducationSection(items = listOf(
                EducationItem(title = "Title 0", institution = "Institution 0", country = "Canada"),
                EducationItem(title = "Title 1", institution = "Institution 1", country = "Canada")
            )))
        } catch (t: Throwable) {
            Assert.fail("Education items with missing institution only is valid")
        }
    }
}
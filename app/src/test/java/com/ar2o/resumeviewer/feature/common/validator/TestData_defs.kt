package com.ar2o.resumeviewer.feature.common.validator

import com.ar2o.resumeviewer.repository.model.*

internal const val EMPTY = ""
internal const val BLANK = "  "

internal val ACHIEVEMENTS = AchievementsSection(
    description = "Some of relevant achievements",
    items = listOf("item 0", "item 1", "item 2")
)

internal val PERSON_CONTACT = PersonContact(
    email = "jdoe@example.com",
    phone = "123-456-7890"
)

internal val PERSON = Person(
    firstName = "John",
    lastName = "DOE",
    title = "Title"
)

internal val PERSON_INFO = PersonInfoSection(
    person = PERSON,
    contact = PERSON_CONTACT
)

internal val QUALIFICATIONS = QualificationsSection(
    description = "Some of relevant qualifications",
    items = listOf("item 0", "item 1", "item 2")
)

internal val EMPLOYMENT_ITEM_INFO = EmploymentItemInfo(
    title = "Title",
    company = "Company",
    city = "City",
    state = "State",
    country = "Country",
    startDate = EmploymentItemDate(month = 1, year = 2000),
    endDate = EmploymentItemDate(month = 10, year = 2005)
)

internal val EMPLOYMENT_ITEM_PROJECT = EmploymentItemProject(
    description = "Description",
    names = listOf("Name 0", "Name 1", "Name 2"),
    items = listOf("Item 0", "Item 1")
)